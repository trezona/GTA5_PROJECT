﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System.Drawing;
using NativeUI;
using System.Collections.Generic;
using Fuel;
using CitizenFX.Core.UI;

namespace Fuel
{
    public class Fuel : BaseScript
    {
        const string fuelLevelPropertyName = "_Fuel_Level";
        const float fuelAccelerationImpact = 0.0002f;
        const float fuelTractionImpact = 0.0001f;
        const float fuelRPMImpact = 0.0002f;

        protected int fuelTankMax;
        protected InLoopOutAnimation jerryCanAnimation;
        public Fuel()
        {
            jerryCanAnimation = new InLoopOutAnimation(
              new Animation("weapon@w_sp_jerrycan", "fire_intro"),
              new Animation("weapon@w_sp_jerrycan", "fire"),
              new Animation("weapon@w_sp_jerrycan", "fire_outro")
            );

            EventHandlers["playerSpawned"] += new Action(() => CreateBlips());
            EventHandlers["onResourceStart"] += new Action(() => CreateBlips());


            Tick += OnTick;
            EntityDecoration.RegisterProperty(fuelLevelPropertyName, DecorationType.Float);
        }

        public void Init(Vehicle vehicle)
        {
            fuelTankMax = PetrolTanks.FuelLevelMax(vehicle);
            if (!vehicle.HasDecor("_Fuel_Level")) vehicle.SetDecor("_Fuel_Level", fuelTankMax * vehicle.FuelLevel / 100);
            vehicle.FuelLevel = vehicle.GetDecor<float>("_Fuel_Level") / fuelTankMax * 100;
        }

        public async Task OnTick()
        {
            Ped playerPed = Game.PlayerPed;

            foreach (var raw in GasStations.Positions) World.DrawMarker(MarkerType.VerticalCylinder, raw.Key - Vector3.UnitZ, Vector3.Zero, Vector3.Zero, Vector3.One, Color.FromArgb(0, 0, 255));

            if (playerPed.IsInVehicle() && playerPed.CurrentVehicle.Model.IsVehicle && playerPed.CurrentVehicle.Driver == playerPed && playerPed.CurrentVehicle.IsAlive)
            {
                if (playerPed.LastVehicle != playerPed.CurrentVehicle) Init(playerPed.CurrentVehicle);
                ConsumeFuel(playerPed.CurrentVehicle);
            }
            else Refuel(playerPed);
            await Task.FromResult(0);
        }
        public void CreateBlips()
        {
            foreach (var raw in GasStations.Positions)
            {
                var blip = World.CreateBlip(raw.Key);
                blip.Sprite = BlipSprite.JerryCan;
                blip.Color = BlipColor.White;
                blip.Scale = 1f;
                blip.IsShortRange = true;
                blip.Name = "Заправка";
            }
        }
        public void ConsumeFuel(Vehicle vehicle)
        {
            float fuel = 0;
            if (vehicle.IsEngineRunning)
            {
                float normalizedRPMValue = (float)Math.Pow(vehicle.CurrentRPM, 1.5);
                fuel -= normalizedRPMValue * fuelRPMImpact;
                fuel -= vehicle.Acceleration * fuelAccelerationImpact;
                fuel -= vehicle.MaxTraction * fuelTractionImpact;
            }
            PetrolTanks.SetFuel(vehicle, fuel);
        }
        public void Refuel(Ped playerPed)
        {
            if (playerPed.Weapons.Current.Hash == WeaponHash.PetrolCan)
            {
                Vector3 pos = playerPed.Position;
                Vehicle vehicle = new Vehicle(Function.Call<int>(Hash.GET_CLOSEST_VEHICLE, pos.X, pos.Y, pos.Z, 3f, 0, 70));
                if (vehicle.Handle != 0 && vehicle.HasDecor(fuelLevelPropertyName))
                {
                    float current = PetrolTanks.FuelLevel(vehicle);

                    if (Game.IsControlPressed(0, Control.Attack))
                    {
                        jerryCanAnimation.Magick(playerPed);
                        PetrolTanks.SetFuel(vehicle, 0.01f);
                    }

                    if (Game.IsControlJustReleased(0, Control.VehicleAttack)) jerryCanAnimation.RewindAndStop(playerPed);
                    return;
                }
                if (Game.IsControlPressed(0, Control.Attack)) Game.PlayerPed.Task.ClearAll();
            }
        }
    }
}

public class Menu : BaseScript
{
    private MenuPool _menuPool;
    private float _fuel = 0;
    public void AddMenuFuels(UIMenu menu)
    {
        List<dynamic> menus = new List<dynamic>() { "0L" };
        for (float i = 0.1f; i < PetrolTanks.FuelLevelMax(Game.PlayerPed.LastVehicle) - PetrolTanks.FuelLevel(Game.PlayerPed.LastVehicle); i += 0.1f) menus.Add($"{i}L");

        var newitem = new UIMenuListItem("Заправить", menus, 0);
        menu.AddItem(newitem);
        menu.OnListChange += (sender, item, index) =>
        {
            if (item == newitem)
            {
                item.Description = $"Стоимость заправки ~r~${index / 10f * 0.4f}";
            }
        };
        menu.OnListSelect += (sender, item, index) =>
        {
            if (item == newitem)
            {
                _fuel = index / 10f;
                Screen.ShowNotification($"Машина заправляется, ~r~на {index / 10f}L");
            }
        };
    }

    UIMenu mainMenu;
    public Menu()
    {
        _menuPool = new MenuPool();
        mainMenu = new UIMenu("", "test");
        mainMenu.SetBannerType(new UIResRectangle());
        mainMenu.MouseControlsEnabled = false;
        mainMenu.ControlDisablingEnabled = false;
        _menuPool.Add(mainMenu);
        _menuPool.RefreshIndex();

        Tick += new Func<Task>(delegate
        {
            _menuPool.ProcessMenus();
            if (Util.IsPlayerNearAnyPump(Game.PlayerPed.Position) && !Game.PlayerPed.IsInVehicle())
            {
                if (Game.PlayerPed.LastVehicle != null)
                {
                    if (Util.IsVehicleNearAnyPump(Game.PlayerPed.LastVehicle) && _fuel <= 0)
                    {
                        if (mainMenu.Visible == true) return Task.FromResult(0);
                        Debug.WriteLine(Util.GetStation(Game.PlayerPed.Position, 5).ToString());
                        mainMenu.Clear();
                        AddMenuFuels(mainMenu);
                        mainMenu.Subtitle.Caption = $"Заправить ~b~{Game.PlayerPed.LastVehicle.DisplayName}";
                        mainMenu.Visible = true;
                        return Task.FromResult(0);
                    }
                }
            }
            mainMenu.Visible = false;
            if (_fuel > 0)
            {
                if (Game.PlayerPed.LastVehicle == null) return Task.FromResult(0); ;
                if (Util.IsVehicleNearAnyPump(Game.PlayerPed.LastVehicle))
                {
                    PetrolTanks.SetFuel(Game.PlayerPed.LastVehicle, 0.01f);
                    _fuel -= 0.01f;
                }
                else _fuel = 0;
            }
            return Task.FromResult(0);
        });
    }
}
