﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System.Linq;

namespace Fuel
{
    public class Util
    {
        static string[] tankBones = new string[] {
            "petrolcap",
            "petroltank",
            "petroltank_r",
            "petroltank_l",
            "wheel_lr"
        };

        static public bool IsPlayerNearAnyPump(Vector3 pos)
        {
            return Vector3.DistanceSquared(GetStation(pos, 20), pos) <= 3f ? true : false;
        }

        static public bool IsVehicleNearAnyPump(Vehicle vehicle)
        {
            foreach (var pump in GetPumps(vehicle.Position))
            {
                if (Vector3.DistanceSquared(pump, GetTankPos(vehicle)) <= 20f) return true;
            }
            return false;
        }
        static public Vector3 GetStation(Vector3 pos, float range = 300)
        {
            foreach (var raw in GasStations.Positions)
                if (Vector3.DistanceSquared(raw.Key, pos) < range) return raw.Key;
            return new Vector3(0, 0, 0);
        }

        static public Vector3[] GetPumps(Vector3 pos, float range = 300)
        {
            foreach (var raw in GasStations.Positions)
                if (Vector3.DistanceSquared(raw.Key, pos) < range) return raw.Value;
            return new Vector3[0];
        }
        static public Vector3 GetTankPos(Vehicle vehicle)
        {
            foreach (var boneName in tankBones)
            {
                var bone = vehicle.Bones[boneName];
                if (bone.IsValid && bone != null) return bone.Position;
            }
            return vehicle.Position;
        }

    }
}
