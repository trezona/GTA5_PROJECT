﻿using CitizenFX.Core;
using FXServer.Core.SyncNet;
using System;

namespace FXServer.Game.Module
{
    public class Init : BaseScript
    {
        public Init()
        {
            Network.onNetworkDataServer += new Network.onNetworkDataServerDelegate((player, trigger, data) =>
            {
                if (trigger == "chat:getMessage")
                {
                    Debug.WriteLine("[NETWORK][GET] TYPE: CHAT; FROM: {0}; MESSAGE: {1};", player.Name, data);
                    Send($"{player.Name}: {data}");
                }
            });
        }
        static public void Send(string message) => Network.Send("chat:message", message);
        static public void Send(string message, Player player) => Network.Send(player, "chat:message", message);
        static public void Clear() => Network.Send("chat:clear");
        static public void Clear(Player player) => Network.Send(player, "chat:clear");
    }
}
