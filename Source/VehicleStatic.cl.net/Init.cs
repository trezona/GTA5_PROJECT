﻿using System;
using FXClient.Core.SyncNet;
using CitizenFX.Core;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Threading.Tasks;
using Components;

namespace VehicleStatic.cl.net
{
    public class Init : BaseScript
    {
        public Init()
        {
            Network.onServerData += ON_GET_CAR;
            Tick += Init_Tick;
            EntityDecoration.RegisterProperty("_STATIC_VEHICLE", DecorationType.Int);
        }

        DateTime lastUpdate = DateTime.Now;
        private Task Init_Tick()
        {
            if (Game.PlayerPed.IsInVehicle() && (DateTime.Now - lastUpdate).TotalMinutes > 1)
            {
                VehicleStaticForClient.Update(Game.PlayerPed.CurrentVehicle);
                lastUpdate = DateTime.Now;
            }

            if (Game.IsControlJustReleased(0, Control.MultiplayerInfo) && Game.PlayerPed.IsInVehicle())
            {
                //Network.Send(new Action(async () => { await VehicleUnit.Create(new Model(VehicleHash.Police3), Game.PlayerPed.Position); }), PlayerUnit.Shared);
                //Network.Send("playerSpawned", PlayerUnit.Shared);
                //if (Game.PlayerPed.CurrentVehicle != null) Debug.WriteLine("POS: {0}, ROT: {1}, MODEL: {2}", Game.PlayerPed.CurrentVehicle.Position, Game.PlayerPed.CurrentVehicle.Rotation, Game.PlayerPed.CurrentVehicle.Model.Hash);
                VehicleStaticForClient newCar = VehicleStaticForClient.Create((VehicleHash)Game.PlayerPed.CurrentVehicle.Model.Hash, Game.PlayerPed.CurrentVehicle.Position, Game.PlayerPed.CurrentVehicle.Rotation);
                newCar.Color1 = Color.FromArgb(0, 0, 0);
                newCar.Color2 = Color.FromArgb(0, 0, 0);
                newCar.Save();
                //Network.Send("playerSpawned", PlayerUnit.Shared);
            }
            return Task.FromResult(true);
        }

        private bool CheckExist(dynamic data)
        {
            if (data.Handle != 0)
            {
                Vehicle obj;
                if ((obj = Vehicle.FromHandle(data.Handle)) != null)
                {
                    if (obj.Model.IsVehicle && obj.Model == new Model((int)data.Model)) return true; //Skip isExist
                }
            }
            return false;
        }

        bool isCreate = false;
        public async void ON_GET_CAR(string triggerName, dynamic data)
        {
            if (triggerName == "vehicleStatic:client:sync" && isCreate == false)
            {
                try
                {
                    if (Game.Player == null || !Game.Player.IsPlaying) return; //Exit
                    isCreate = true;

                    Dictionary<int, dynamic> _newDataArray = new Dictionary<int, dynamic>();
                    foreach (var _CarData in data as List<dynamic>)
                    {
                        try
                        {
                            if (CheckExist(_CarData.Value)) continue;
                            Vehicle obj = await World.CreateVehicle(new Model((int)_CarData.Value.Model), Vector3.Zero, 0);
                            VehicleStaticForClient.ApplyData(obj, _CarData.Key, _CarData.Value);
                            _newDataArray[_CarData.Key] = new Dictionary<string, dynamic>() { { "ID", _CarData.Key }, { "Handle", obj.Handle } }.ToArray();

                        }
                        catch (Exception e) { Debug.WriteLine(e.Message); }
                    }
                    if (_newDataArray.Count > 0) Network.Server<dynamic>("vehicleStatic:update", _newDataArray.ToArray());
                }
                catch (Exception e) { Debug.WriteLine(e.Message); }
                isCreate = false;
            }
        }
    }
}
