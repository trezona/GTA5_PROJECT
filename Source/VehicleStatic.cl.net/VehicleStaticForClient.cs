﻿using CitizenFX.Core;
using FXClient.Core.SyncNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Components
{
    public class VehicleStaticExtra : Dictionary<int, bool> { }
    public class VehicleStaticMods : Dictionary<int, int>
    {
        public bool TireSmoke { set => this[(VehicleModType)20] = value ? 1 : 0; }
        public bool Turbo { set => this[(VehicleModType)18] = value ? 1 : 0; }
        public bool XenonHeadlights { set => this[(VehicleModType)22] = value ? 1 : 0; }
        public int this[VehicleModType index]
        {
            get => this[(int)index];
            set => this[(int)index] = value;
        }
    }
    public class VehicleStaticForClient : Dictionary<string, dynamic>
    {
        public VehicleStaticForClient()
        {
            this["ID"] = -1;
            this["Handle"] = 0;
            this["Model"] = (uint)VehicleHash.Blista;
            this["Fuel"] = 65;
            this["OilLevel"] = 65;
            this["Heading"] = 0;
            this["Color1"] = new byte[3] { 0, 0, 0 };
            this["Color2"] = new byte[3] { 0, 0, 0 };
            this["Pos"] = new float[3] { 0, 0, 0 };
            this["Rot"] = new float[3] { 0, 0, 0 };
            this["Lock"] = (int)VehicleLockStatus.Unlocked;
            this["Persistent"] = false;
        }
        static public VehicleStaticForClient Create(VehicleHash model, Vector3 position, float heading = 0)
        {
            var newStatic = new VehicleStaticForClient();
            newStatic.Model = model;
            newStatic.Position = position;
            newStatic.Heading = heading;
            return newStatic;
        }
        static public VehicleStaticForClient Create(VehicleHash model, Vector3 position, Vector3 rotation)
        {
            var newStatic = new VehicleStaticForClient();
            newStatic.Model = model;
            newStatic.Position = position;
            newStatic.Rotation = rotation;
            return newStatic;
        }

        public int ID { get => this["ID"]; set => this["ID"] = value; }
        public int Handle { set => this["Handle"] = value; }
        public float Heading { set => this["Heading"] = value; }
        public VehicleHash Model { set => this["Model"] = (uint)value; }
        public Vector3 Position { set => this["Pos"] = new float[3] { value.X, value.Y, value.Z }; }
        public Vector3 Rotation { set => this["Rot"] = new float[3] { value.X, value.Y, value.Z }; }
        public Color Color1 { set { this["Color1"] = new byte[3] { value.R, value.G, value.B }; } }
        public Color Color2 { set { this["Color2"] = new byte[3] { value.R, value.G, value.B }; } }
        public float FuelLevel { set { this["Fuel"] = value; } }
        public float OilLevel { set { this["OilLevel"] = value; } }
        public bool IsPersistent { set { this["Persistent"] = value; } }
        public VehicleStaticMods Mods { get; } = new VehicleStaticMods();
        public VehicleStaticExtra Extra { get; } = new VehicleStaticExtra();
        public VehicleLockStatus Lock { set => this["Lock"] = (int)value; }
        public static void Update(Vehicle obj)
        {
            if (!obj.IsPersistent) return;
            var update = Create((VehicleHash)obj.Model.Hash, obj.Position, obj.Rotation);
            update.FuelLevel = obj.FuelLevel;
            update.OilLevel = obj.OilLevel;
            update.Handle = obj.Handle;
            update.ID = obj.HasDecor("_STATIC_VEHICLE") ? obj.GetDecor<int>("_STATIC_VEHICLE") : -1;
            update.Rotation = obj.Rotation;
            update.Position = obj.Position;
            //update.Lock = obj.LockStatus; //Break Client
            update.Color1 = obj.Mods.CustomPrimaryColor;
            update.Color2 = obj.Mods.CustomSecondaryColor;
            update.Model = (VehicleHash)obj.Model.Hash;
            foreach (var _mod in obj.Mods.GetAllMods()) update.Mods[_mod.ModType] = _mod.Index;
            for (int i = 0; i < 20; i++) update.Extra[i] = obj.IsExtraOn(i);
            update["Mods"] = update.Mods.ToArray();
            update["Extra"] = update.Extra.ToArray();
            Network.Server<dynamic>("vehicleStatic:update", new Dictionary<int, dynamic> { { update.ID, update.ToArray() } }.ToArray());
        }
        public static void ApplyData(Vehicle obj, int key, dynamic data)
        {
            obj.Position = new Vector3(data.Pos[0], data.Pos[1], data.Pos[2]);
            if (data.Heading > 0 || data.Heading < 0) obj.Heading = data.Heading;
            else obj.Rotation = new Vector3(data.Rot[0], data.Rot[1], data.Rot[2]);
            obj.Mods.CustomPrimaryColor = Color.FromArgb((byte)data.Color1[0], (byte)data.Color1[1], (byte)data.Color1[2]);
            obj.Mods.CustomSecondaryColor = Color.FromArgb((byte)data.Color2[0], (byte)data.Color2[1], (byte)data.Color2[2]);
            obj.Mods.InstallModKit();
            obj.FuelLevel = (float)data.Fuel;
            obj.FuelLevel = (float)data.OilLevel;
            obj.LockStatus = (VehicleLockStatus)data.Lock;
            obj.SetDecor("_STATIC_VEHICLE", key);
            obj.IsPersistent = data.Persistent;
            //Write
            foreach (var _mod in data.Mods as List<dynamic>)
            {
                switch (_mod.Key)
                {
                    case VehicleToggleModType.TireSmoke:
                        obj.Mods[(VehicleToggleModType)_mod.Key].IsInstalled = _mod.Value == 1;
                        break;
                    case VehicleToggleModType.Turbo:
                        obj.Mods[(VehicleToggleModType)_mod.Key].IsInstalled = _mod.Value == 1;
                        break;
                    case VehicleToggleModType.XenonHeadlights:
                        obj.Mods[(VehicleToggleModType)_mod.Key].IsInstalled = _mod.Value == 1;
                        break;
                    default:
                        obj.Mods[(VehicleModType)_mod.Key].Variation = true;
                        obj.Mods[(VehicleModType)_mod.Key].Index = _mod.Value;
                        break;
                }
            }
            foreach (var _extra in data.Extra as List<dynamic>) obj.ToggleExtra(_extra.Key, _extra.Value);
        }
        public void Save()
        {
            this["Mods"] = Mods.ToArray();
            this["Extra"] = Extra.ToArray();
            Network.Server<dynamic>("vehicleStatic:add", this.ToArray());
        }
    }
}
