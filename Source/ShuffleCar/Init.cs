﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using LibSharp.CSharpCore.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShuffleCar
{
    public class Init : Core
    {
        private Player Player { get; } = Game.Player;
        private Vehicle Vehicle;
        public override void ResourceStart()
        {
            //ShufleVehicle
            Tick += new Func<Task>(() =>
            {
                if (Game.IsControlJustReleased(0, Control.MultiplayerInfo)) Function.Call((Hash)0xba970511, LocalPlayer.Character.CurrentVehicle.Handle, 20f);
                if (Player.Character.IsInVehicle() && Player.Character.IsAlive)
                {
                    var seat = Player.Character.IsSeatIndex();
                    if (seat == VehicleSeat.Driver || seat == VehicleSeat.Passenger)
                    {
                        if (Player.Character.CurrentVehicle.IsSeatFree(VehicleSeat.Driver)) Player.Character.Task.ClearAll();
                    }
                }
                return Task.FromResult(true);
            });
            //Abort Entered Vehicle
            Tick += new Func<Task>(() =>
            {
                if (Game.PlayerPed.VehicleTryingToEnter != null)
                {
                    if (Game.IsControlJustPressed(0, Control.MoveUpOnly) ||
                            Game.IsControlJustPressed(0, Control.MoveDownOnly) ||
                            Game.IsControlJustPressed(0, Control.MoveLeftOnly) ||
                            Game.IsControlJustPressed(0, Control.MoveRightOnly))
                        LocalPlayer.Character.Task.ClearAll();
                }
                return Task.FromResult(true);
            });
            //Event
            onEnteringVehicle += EnteringVehicle;
        }

        void EnterTo(Dictionary<float, VehicleSeat> inSeat)
        {
            foreach (var raw in inSeat.OrderBy(u => u.Key))
            {
                if (raw.Value == VehicleSeat.Driver && Game.IsControlPressed(0, Control.Detonate)) return;
                if (Vehicle.IsSeatFree(raw.Value) || !Vehicle.GetPedOnSeat(VehicleSeat.Driver).IsPlayer)
                {
                    Player.Character.Task.ClearAll();//Reset default enter Vehicle
                    Player.Character.Task.EnterVehicle(Vehicle, raw.Value);
                    return;
                }
            }
            EnterFree();
        }
        void EnterFree()
        {
            for (int i = 0; i <= Vehicle.PassengerCapacity; i++)
            {
                if (!Vehicle.IsSeatFree((VehicleSeat)i)) continue;
                Player.Character.Task.ClearAll();//Reset default enter Vehicle
                Player.Character.Task.EnterVehicle(Vehicle, (VehicleSeat)i);
                return;
            }
        }
        void EnteringVehicle(Vehicle Vehicle, VehicleSeat seat)
        {
            this.Vehicle = Vehicle;
            Dictionary<float, VehicleSeat> inSeat = new Dictionary<float, VehicleSeat>();

            if (Vehicle.PassengerCapacity == 0) return;
            else if (Vehicle.Model.IsBicycle || Vehicle.Model.IsBike || Vehicle.Model.IsQuadbike)
            {
                if (Game.IsControlPressed(0, Control.Detonate)) inSeat[0] = VehicleSeat.Driver;
                else inSeat[0] = VehicleSeat.Passenger;
            }
            else if (Vehicle.PassengerCapacity == 1)
            {
                inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + Vehicle.Left())] = VehicleSeat.Driver;
                inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + Vehicle.Right())] = VehicleSeat.Passenger;
            }
            else if (Vehicle.PassengerCapacity == 3)
            {
                if (Game.IsControlPressed(0, Control.Detonate)) inSeat[0] = VehicleSeat.Driver;
                //Front
                inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Left() * 2 + Vehicle.Forward()))] = VehicleSeat.Driver;
                inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Right() * 2 + Vehicle.Forward()))] = VehicleSeat.Passenger;
                //Rear
                inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Left() * 2 + Vehicle.Backward() * 4f))] = VehicleSeat.LeftRear;
                inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Right() * 2 + Vehicle.Backward() * 4f))] = VehicleSeat.RightRear;
            }
            else if (Vehicle.PassengerCapacity == 7)
            {
                if (Game.IsControlPressed(0, Control.Detonate))
                {
                    //Extra Front
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Left() * 2 + Vehicle.Forward()))] = VehicleSeat.ExtraSeat1;
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Right() * 2 + Vehicle.Forward()))] = VehicleSeat.ExtraSeat2;
                    //Extra Rear
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Left() * 2 + Vehicle.Backward() * 4f))] = VehicleSeat.ExtraSeat3;
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Right() * 2 + Vehicle.Backward() * 4f))] = VehicleSeat.ExtraSeat4;
                }
                else
                {
                    //Front
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Left() * 2 + Vehicle.Forward()))] = VehicleSeat.Driver;
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Right() * 2 + Vehicle.Forward()))] = VehicleSeat.Passenger;
                    //Rear
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Left() * 2 + Vehicle.Backward() * 4f))] = VehicleSeat.LeftRear;
                    inSeat[LocalPlayer.Character.Position.DistanceToSquared(Vehicle.Position + (Vehicle.Right() * 2 + Vehicle.Backward() * 4f))] = VehicleSeat.RightRear;
                }
            }
            else if (Vehicle.PassengerCapacity > 7)
            {
                if (Game.IsControlPressed(0, Control.Detonate)) inSeat[0] = VehicleSeat.Driver;
                else
                {
                    EnterFree();
                    return;
                }
            }
            EnterTo(inSeat);
        }

    }
}
