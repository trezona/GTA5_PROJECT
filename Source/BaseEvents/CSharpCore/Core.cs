﻿using CitizenFX.Core;
using System;
using System.Threading.Tasks;
using CitizenFX.Core.Native;

namespace LibSharp.CSharpCore
{
    public class Core : BaseScript
    {
        static public void Call(Hash _hash, params InputArgument[] _args) => Function.Call(_hash, _args);
        static public T Call<T>(Hash _hash, params InputArgument[] _args) => Function.Call<T>(_hash, _args);
        static public void Call(ulong _hash, params InputArgument[] _args) => Function.Call((Hash)_hash, _args);
        static public T Call<T>(ulong _hash, params InputArgument[] _args) => Function.Call<T>((Hash)_hash, _args);

        public Core()
        {
            Tick += Update;
            EventHandlers["onResourceStart"] += new Action(ResourceStart);
            EventHandlers["onResourceStop"] += new Action(ResourceStop);
            EventHandlers["playerSpawned"] += new Action(PlayerSpawned);

            Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onEnteringVehicle") onEnteringVehicle(new Vehicle(data[0]), (VehicleSeat)data[1]); });
            Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onEnteringAborted") onEnteringAborted(new Vehicle(data[0]), (VehicleSeat)data[1]); });
            Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onEnteredVehicle") onEnteredVehicle(new Vehicle(data[0]), (VehicleSeat)data[1]); });
            Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onLeftVehicle") onLeftVehicle(new Vehicle(data[0]), (VehicleSeat)data[1]); });
            Network.onLocalData += new Network.LocalDataDelegate((trigger, data) => { if (trigger == "onPlayerKilled") onPlayerKilled(data); });
            Network.onLocalTrigger += new Network.LocalTriggerDelegate((trigger) => { if (trigger == "onPlayerDied") onPlayerDied(); });
            Network.onLocalTrigger += new Network.LocalTriggerDelegate((trigger) => { if (trigger == "onPlayerWasted") onPlayerWasted(); });
        }


        #region EVENTS
        public delegate void PlayerKilledDelegate(int killer);
        public delegate void VehicleDelegate(Vehicle vehicle, VehicleSeat seat);
        public delegate void EmptyDelegate();
        public static event VehicleDelegate onEnteringVehicle = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
        public static event VehicleDelegate onEnteringAborted = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
        public static event VehicleDelegate onEnteredVehicle = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
        public static event VehicleDelegate onLeftVehicle = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
        public static event VehicleDelegate onLeftVehicleJump = new VehicleDelegate(new VehicleDelegate((vehicle, seat) => { }));
        public static event EmptyDelegate onPlayerDied = new EmptyDelegate(new EmptyDelegate(() => { }));
        public static event EmptyDelegate onPlayerWasted = new EmptyDelegate(new EmptyDelegate(() => { }));
        public static event PlayerKilledDelegate onPlayerKilled = new PlayerKilledDelegate(new PlayerKilledDelegate((killer) => { }));
        #endregion

        virtual public async Task Update() { await Delay(0); }
        virtual public void ResourceStart() { }
        virtual public void ResourceStop() { }
        virtual public void PlayerSpawned() { }
    }

    public enum Keys
    {
        ESC = 322, F1 = 288, F2 = 289, F3 = 170, F5 = 166, F6 = 167, F7 = 168, F8 = 169, F9 = 56, F10 = 57,
        Tilde = 243, D1 = 157, D2 = 158, D3 = 160, D4 = 164, D5 = 165, D6 = 159, D7 = 161, D8 = 162, D9 = 163, Minus = 84, Plus = 83, BACKSPACE = 177,
        TAB = 37, Q = 44, W = 32, E = 38, R = 45, T = 245, Y = 246, U = 303, P = 199, OpenBrackets = 39, CloseBrackets = 40, ENTER = 18,//OpenBrackets = [, CloseBrackets = ]
        CAPS = 137, A = 34, S = 8, D = 9, F = 23, G = 47, H = 74, K = 311, L = 182,
        LEFTSHIFT = 21, Z = 20, X = 73, C = 26, V = 0, B = 29, N = 249, M = 244, PointLeft = 82, Point = 81,//PointLeft = , Point = .
        LEFTCTRL = 36, LEFTALT = 19, SPACE = 22, RIGHTCTRL = 70,
        HOME = 213, PAGEUP = 10, PAGEDOWN = 11, DELETE = 178,
        LEFT = 174, RIGHT = 175, TOP = 27, DOWN = 173,
        NENTER = 201, N4 = 108, N5 = 60, N6 = 107, NPlus = 96, NMinus = 97, N7 = 117, N8 = 61, N9 = 118
    }
}