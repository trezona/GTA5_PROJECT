﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using FXServer.Core.Components;
using FXServer.Core.Components.Models;
using System;

namespace FXServer.Core.Network
{
    class RecivePacket : BaseScript
    {
        public RecivePacket()
        {
            //EventHandlers["chat:messageCancel"]
            EventHandlers["FXServer:changeSkin"] += new Action<Player, uint>(FXServer_changeSkin);
            EventHandlers["FXServer:sendMessage"] += new Action<Player, string, int>(FXServer_sendMessage);
            EventHandlers["FXServer:repairVehicle"] += new Action<Player, int>(FXServer_repairVehicle);
            EventHandlers["FXServer:runFunction"] += new Action<Player, dynamic, int>(FXServer_runFunction);
        }

        private void FXServer_runFunction([FromSource] Player player, dynamic action, int toPlayer)
        {

            if (toPlayer >= 0)
            {
                Debug.WriteLine("[NETWORK][GET] FUNC-RUN: **** FROM: {0} FOR: {1}", player.Name, Client.From(toPlayer).Name);
                SendNetClient.RunFunction(action, Client.From(toPlayer));
                return;
            }
            Debug.WriteLine("[NETWORK][GET] FUNC-RUN: **** FROM: {0}", player.Name);
            SendNetClient.RunFunction(action);
        }
        private void FXServer_repairVehicle([FromSource] Player player, int vehicle)
        {
            Debug.WriteLine("[NETWORK][GET] VEH-REPAIR: {0} FROM: {1}", vehicle, player.Name);
            SendNetClient.RepairVehicle(Vehicle.From(vehicle));
        }
        private void FXServer_sendMessage([FromSource] Player player, string message, int toPlayer)
        {
            if (toPlayer >= 0)
            {
                Debug.WriteLine("[NETWORK][GET] CHAT-MESSAGE: {0} FROM: {1} FOR: {2}", message, player.Name, Client.From(toPlayer).Name);
                SendNetClient.SendChatMessage(message, Client.From(toPlayer));
                return;
            }
            Debug.WriteLine("[NETWORK][GET] CHAT-MESSAGE: {0} FROM: {1}", message, player.Name);
            SendNetClient.SendChatMessage(message);
        }
        private void FXServer_changeSkin([FromSource] Player player, uint model)
        {
            Debug.WriteLine("[NETWORK][GET] SET-SKIN: {0} FROM: {1}", model, player.Name);
            SendNetClient.ChangeModel(Client.From(player), (PedHash)model);
        }
    }
}
