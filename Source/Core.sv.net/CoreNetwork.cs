﻿using CitizenFX.Core;
using System;

namespace Server.SyncNet
{
    public class CoreNetwork : BaseScript
    {
        bool onDebug = false;
        public void WriteLine(string format, params object[] args) { if (onDebug) Debug.WriteLine(format, args); }

        public CoreNetwork()
        {
            EventHandlers["FX:SERVER:CLIENT:DATA"] += new Action<Player, string, dynamic>(FX_SERVER_FROM_CLIENT_DATA); //Ready
            EventHandlers["FX:SERVER:CLIENT:TRIGGER"] += new Action<Player, string>(FX_SERVER_FROM_CLIENT_TRIGGER); //Ready
            EventHandlers["FX:SERVER:TRIGGER"] += new Action<Player, string, int>(TRIGGER_SYNC); //Ready
            EventHandlers["FX:SERVER:DATA"] += new Action<Player, string, dynamic, int>(DATA_SYNC); //Ready
            EventHandlers["FX:SERVER:PACKET"] += new Action<Player, dynamic, int>(FX_SERVER_PACKET); //Ready
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>(trigger => WriteLine("[LOCAL][GET] TRIGGER: {0}", trigger));
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>((trigger, data) => WriteLine("[LOCAL][GET] EVENT: {0}; DATA: {1}", trigger, data));
        }

        private void FX_SERVER_FROM_CLIENT_TRIGGER([FromSource] Player fromPlayer, string triggerName) => WriteLine("[SERVER-NET][GET] FROM: {0}, TRIGGER: {1}", fromPlayer.Name, triggerName);
        private void FX_SERVER_FROM_CLIENT_DATA([FromSource] Player fromPlayer, string triggerName, dynamic data) => WriteLine("[SERVER-NET][GET] FROM: {0}, EVENT: {1}, DATA: {2}", fromPlayer.Name, triggerName, data);
        private void TRIGGER_SYNC([FromSource] Player fromPlayer, string triggerName, int toPlayer)
        {
            var toClient = Players[toPlayer];
            if (toClient.Handle == fromPlayer.Handle)
            {
                WriteLine("[NETWORK][GET] FROM: {0}, TRIGGER: {1}", fromPlayer.Name, triggerName);
                TriggerClientEvent("FX:CLIENT:TRIGGER", fromPlayer.Handle, triggerName);
                WriteLine("[NETWORK][SEND] TRIGGER: {0}, FROM: {1}", triggerName, fromPlayer.Name);
            }
            else
            {
                WriteLine("[NETWORK][GET] FROM: {0}, TRIGGER: {1}, FOR: {2}", fromPlayer.Name, triggerName, toClient.Name);
                TriggerClientEvent(toClient, "FX:CLIENT:TRIGGER", fromPlayer.Handle, triggerName);
                WriteLine("[NETWORK][SEND] TRIGGER: {0}, FROM: {1}, FOR: {2}", triggerName, fromPlayer.Name, toClient.Name);
            }
        }
        private void DATA_SYNC([FromSource] Player fromPlayer, string triggerName, dynamic data, int toPlayer)
        {
            var toClient = Players[toPlayer];
            if (toClient.Handle == fromPlayer.Handle)
            {
                WriteLine("[NETWORK][GET] FROM: {0}, EVENT: {1}, DATA: {2}", fromPlayer.Name, triggerName, data);
                TriggerClientEvent("FX:CLIENT:DATA", fromPlayer.Handle, triggerName, data);
                WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FROM: {3}", triggerName, data.GetType(), data, fromPlayer.Name);
            }
            else
            {
                WriteLine("[NETWORK][GET] FROM: {0}; EVENT: {1}; DATA: {2}, FOR: {3}", fromPlayer.Name, triggerName, data, toClient.Name);
                TriggerClientEvent(toClient, "FX:CLIENT:DATA", fromPlayer.Handle, triggerName, data);
                WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FROM: {3}, FOR: {4}", triggerName, data.GetType(), data, fromPlayer.Name, toClient.Name);
            }
        }
        private void FX_SERVER_PACKET([FromSource] Player fromPlayer, dynamic data, int toPlayer)
        {
            var toClient = Players[toPlayer];
            if (toClient.Handle == fromPlayer.Handle)
            {
                WriteLine("[NETWORK][GET] FROM: {0}, PACKET: {1}", fromPlayer.Name, data);
                TriggerClientEvent("FX:CLIENT:PACKET", fromPlayer.Handle, data);
                WriteLine("[NETWORK][SEND] PACKET: {0}, FROM: {1}", data, fromPlayer.Name);
            }
            else
            {
                WriteLine("[NETWORK][GET] FROM: {0}, PACKET: {1}, FOR: {2}", fromPlayer.Name, data, toClient.Name);
                TriggerClientEvent(toClient, "FX:CLIENT:PACKET", fromPlayer.Handle, data);
                WriteLine("[NETWORK][SEND] PACKET: {0}, FROM: {1}, FOR: {2}", data, fromPlayer.Name, toClient.Name);
            }
        }
    }
}
