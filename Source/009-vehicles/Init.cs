﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_vehicles
{
    public class Init : Core
    {
        public override void ResourceStart()
        {
            Network.onServerData += Network_onServerData;
        }

        private void Network_onServerData(string triggerName, dynamic data)
        {
            if (triggerName != "staticVehicle:sendVehicle") return;
            //var veh = new VehicleObject(_data);
            //if (veh.Exist()) continue;
            Debug.WriteLine("{0}", data.Model);
        }

        public override Task Update()
        {
            API.GetVehicleFuelLevel
            API.SetVehicleFuelLevel
            if (Game.IsControlJustReleased(0, Control.MultiplayerInfo)) send();
            return base.Update();
        }

        void send()
        {
            var pos = Game.PlayerPed.CurrentVehicle.Position;
            var rot = Game.PlayerPed.CurrentVehicle.Rotation;
            Network.Server("vehicleStatic:Request", pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z);
        }
    }
}
