﻿public static class CFG_GAME
{
    static public bool ENABLE_FRIENDLY_FIRE { get; set; } = true;
    static public bool ENABLE_DISPATCH_SERVICE { get; set; } = false;
    static public float PED_DENSITY_MULTIPLIER { get; set; } = 1;
    static public float PARKED_VEHICLE_DENSITY_MULTIPLIER { get; set; } = 0;
    static public float RANDOM_VEHICLE_DENSITY_MULTIPLIER { get; set; } = 1;
    static public float VEHICLE_DENSITY_MULTIPLIER { get; set; } = 1;
    static public bool CLEAR_AREA_OF_COPS { get; set; } = true;
}
