﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using System;
using System.Threading.Tasks;

namespace game_cfg.net
{
    public class Init : Core
    {
        public override void PlayerSpawned()
        {
            foreach (var raw in Enum.GetValues(typeof(eDispatchServiceType))) Function.Call(Hash.ENABLE_DISPATCH_SERVICE, (int)raw, CFG_GAME.ENABLE_DISPATCH_SERVICE);
            // for (int i = 0; i < 150; i++) Function.Call(Hash.DISABLE_POLICE_RESTART, i, false);
            Function.Call(Hash.NETWORK_SET_FRIENDLY_FIRE_OPTION, CFG_GAME.ENABLE_FRIENDLY_FIRE);
            Function.Call(Hash.SET_CREATE_RANDOM_COPS, false);
            Function.Call(Hash.SET_CREATE_RANDOM_COPS_NOT_ON_SCENARIOS, false);
            Function.Call(Hash.SET_CREATE_RANDOM_COPS_ON_SCENARIOS, false);
            Game.Player.DispatchsCops = true;
            Game.MaxWantedLevel = 0;
        }

        public override Task Update()
        {
            var playerPos = Game.PlayerPed.Position;
            Function.Call(Hash.SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, CFG_GAME.VEHICLE_DENSITY_MULTIPLIER);
            Function.Call(Hash.SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, CFG_GAME.RANDOM_VEHICLE_DENSITY_MULTIPLIER);
            Function.Call(Hash.SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME, CFG_GAME.PARKED_VEHICLE_DENSITY_MULTIPLIER);
            Function.Call(Hash.SET_PED_DENSITY_MULTIPLIER_THIS_FRAME, CFG_GAME.PED_DENSITY_MULTIPLIER);
            Function.Call(Hash.CLEAR_AREA_OF_COPS, playerPos.X, playerPos.Y, playerPos.Z, 400, 0);
            Delay(50); return base.Update();
        }

    }


}
