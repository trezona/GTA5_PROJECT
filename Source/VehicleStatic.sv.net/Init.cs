﻿using CitizenFX.Core;
using FXServer.Core.SyncNet;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using System.Collections;

namespace VehicleStatic.sv.net
{
    public class Init : BaseScript
    {
        static public int AI = 0;
        static public Dictionary<int, Hashtable> CarData = new Dictionary<int, Hashtable>();
        public Init()
        {
            Tick += Update;
            Network.onNetworkDataServer += VEH_ADD;
            Network.onNetworkDataServer += VEH_UPDATE;
            Network.onNetworkDataServer += VEH_DELETE;
            EventHandlers["vehicleStatic:add"] += new Action<dynamic>(ADD_VEHICLE);
        }

        private void ADD_VEHICLE(dynamic data)
        {
            Hashtable _newData = new Hashtable();
            foreach (var _data in data as List<dynamic>)
            {
                _newData[(string)_data.Key] = _data.Value;
            }
            CarData[AI] = _newData;
            AI++;
            lastUpdate = lastUpdate.AddMinutes(-10);
        }

        private DateTime lastUpdate = DateTime.Now;
        private Task Update()
        {
            if (Players.Count() == 0 || CarData.Count == 0) return Task.FromResult(true);
            if ((DateTime.Now - lastUpdate).TotalSeconds > 15)
            {
                Network.Send<dynamic>(Players.First(), "vehicleStatic:client:sync", CarData.ToArray());
                lastUpdate = DateTime.Now;
            }
            return Task.FromResult(true);
        }

        private void VEH_DELETE(Player client, string triggerName, dynamic data)
        {
            if (triggerName == "vehicleStatic:delete")
            {
                var key = CarData.First(u => u.Value["HANDLE"] == data).Key;
                CarData.Remove(key);
            }
        }
        private void VEH_UPDATE(Player client, string triggerName, dynamic data)
        {
            if (triggerName == "vehicleStatic:update")
            {
                foreach (var _CarData in data as List<dynamic>) //Key, Value (Car)
                {
                    if(_CarData.Key == -1) continue;
                    foreach (var _data in _CarData.Value as List<dynamic>) //Key, Value (CarData)
                    {
                        CarData[_CarData.Key][_data.Key] = _data.Value;
                    }
                }
            }
        }
        private void VEH_ADD(Player client, string triggerName, dynamic data)
        {
            if (triggerName == "vehicleStatic:add") ADD_VEHICLE(data);
        }
    }
}
