﻿using CitizenFX.Core;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Components
{
    public class VehicleStaticExtra : Dictionary<int, bool> { }
    public class VehicleStaticMods : Dictionary<int, int>
    {
        public bool TireSmoke { set => this[20] = value ? 1 : 0; }
        public bool Turbo { set => this[18] = value ? 1 : 0; }
        public bool XenonHeadlights { set => this[22] = value ? 1 : 0; }
    }
    public class VehicleStaticForClient : Dictionary<string, dynamic>
    {
        public VehicleStaticForClient()
        {
            this["ID"] = -1;
            this["Handle"] = 0;
            this["Model"] = (uint)3950024287;
            this["Fuel"] = 65;
            this["OilLevel"] = 65;
            this["Heading"] = 0;
            this["Color1"] = new byte[3] { 0, 0, 0 };
            this["Color2"] = new byte[3] { 0, 0, 0 };
            this["Pos"] = new float[3] { 0, 0, 0 };
            this["Rot"] = new float[3] { 0, 0, 0 };
            this["Lock"] = 1; //Unlock
            this["Persistent"] = false;
        }
        static public VehicleStaticForClient Create(uint model, Vector3 position, float heading = 0)
        {
            var newStatic = new VehicleStaticForClient();
            newStatic.Model = model;
            newStatic.Position = position;
            newStatic.Heading = heading;
            return newStatic;
        }
        static public VehicleStaticForClient Create(uint model, Vector3 position, Vector3 rotation)
        {
            var newStatic = new VehicleStaticForClient();
            newStatic.Model = model;
            newStatic.Position = position;
            newStatic.Rotation = rotation;
            return newStatic;
        }

        public int ID { get => this["ID"]; set => this["ID"] = value; }
        public int Handle { set => this["Handle"] = value; }
        public float Heading { set => this["Heading"] = value; }
        public uint Model { set => this["Model"] = (uint)value; }
        public Vector3 Position { set => this["Pos"] = new float[3] { value.X, value.Y, value.Z }; }
        public Vector3 Rotation { set => this["Rot"] = new float[3] { value.X, value.Y, value.Z }; }
        public Color Color1 { set { this["Color1"] = new byte[3] { value.R, value.G, value.B }; } }
        public Color Color2 { set { this["Color2"] = new byte[3] { value.R, value.G, value.B }; } }
        public float FuelLevel { set { this["Fuel"] = value; } }
        public float OilLevel { set { this["OilLevel"] = value; } }
        public bool IsPersistent { set { this["Persistent"] = value; } }
        public VehicleStaticMods Mods { get; } = new VehicleStaticMods();
        public VehicleStaticExtra Extra { get; } = new VehicleStaticExtra();
        public int Lock { set => this["Lock"] = (int)value; }
        public void Save()
        {
            this["Mods"] = Mods.ToArray();
            this["Extra"] = Extra.ToArray();
            BaseScript.TriggerEvent("vehicleStatic:add", this.ToArray());
        }
    }
}
