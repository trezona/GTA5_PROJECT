﻿using CitizenFX.Core;
using System;
using System.Threading.Tasks;
using CitizenFX.Core.Native;

namespace LibSharp.CSharpCore
{
    public class Core : BaseScript
    {
        static public void Call(Hash _hash, params InputArgument[] _args) => Function.Call(_hash, _args);
        static public T Call<T>(Hash _hash, params InputArgument[] _args) => Function.Call<T>(_hash, _args);
        static public void Call(ulong _hash, params InputArgument[] _args) => Function.Call((Hash)_hash, _args);
        static public T Call<T>(ulong _hash, params InputArgument[] _args) => Function.Call<T>((Hash)_hash, _args);

        public Core()
        {
            Tick += Update;
            EventHandlers["onResourceStart"] += new Action(ResourceStart);
            EventHandlers["onResourceStop"] += new Action(ResourceStop);
        }

        virtual public async Task Update() { await Delay(0); }
        virtual public void ResourceStart() { }
        virtual public void ResourceStop() { }
    }
}