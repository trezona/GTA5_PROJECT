﻿using CitizenFX.Core;
using FXServer.Core.SyncNet;
using System;

namespace FXServer.Game.Module
{
    public class EVENT_CHAT : BaseScript
    {
        public delegate void onChatMessageDelegate([FromSource] Player player, string message);
        private static void onChatMessageHandler([FromSource] Player player, string message)
        {
            Debug.WriteLine("[NETWORK][GET] TYPE: CHAT; FROM: {0}; MESSAGE: {1};", player.Name, message);
            Send($"{player.Name}: {message}");
        }
        public static event onChatMessageDelegate onChatMessage = new onChatMessageDelegate(onChatMessageHandler);

        public delegate void onChatInitDelegate([FromSource] Player player);
        private static void onChatInitHandler([FromSource] Player player) => Debug.WriteLine("[NETWORK][GET] TYPE: CHAT-INIT; FROM: {0};", player.Name);
        public static event onChatInitDelegate onChatInit = new onChatInitDelegate(onChatInitHandler);

        public EVENT_CHAT()
        {
            EventHandlers["chat:init"] += new Action<Player>(onChatInitHandler);
            EventHandlers["chat:getMessage"] += new Action<Player, string>(onChatMessage);
            //EventHandlers["chat:cancelMessage"] += new Action<Player, string>();
            Network.onNetworkData += new Network.onNetworkDataDelegate((from, trigger, data) => { if (trigger == "chat:Message") Send(data); });
            Network.onNetworkDataPrivate += new Network.onNetworkDataPrivateDelegate((from, to, trigger, data) => { if (trigger == "chat:Message") Send(data, to.Player); });
            Network.onNetworkTrigger += new Network.onNetworkTriggerDelegate((player, trigger) => { if (trigger == "chat:Clear") Clear(); });
            Network.onNetworkTriggerPrivate += new Network.onNetworkTriggerPrivateDelegate((from, to, trigger) => { if (trigger == "chat:Clear") Clear(to.Player); });
        }

        static public void Send(string message) => TriggerClientEvent("chat:message", message);
        static public void Send(string message, Player player) => TriggerClientEvent(player, "chat:message", message);
        static public void Clear() => TriggerClientEvent("chat:clear");
        static public void Clear(Player player) => TriggerClientEvent(player, "chat:clear");
    }
}
