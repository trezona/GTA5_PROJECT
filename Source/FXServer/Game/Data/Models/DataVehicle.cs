﻿namespace FXServer.Core.Data.Models
{
    public class DataVehicle
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string VehicleId { get; set; }
    }
}
