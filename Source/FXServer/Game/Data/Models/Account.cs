﻿using CitizenFX.Core;
using System;

namespace FXServer.Core.Data.Models
{
    public class Account
    {
        public string Id { get; set; }
        public string UID { get; set; }
        public string Steam { get; set; }
        public string Email { get; set; }
        public string Passwd { get; set; }
        public string Name { get; set; }
        public string Components { get; set; } = "{ }";
        public int Health { get; set; } = 100;
        public Vector3 Position { get; set; } = Vector3.Zero;
        public Vector3 Rotation { get; set; } = Vector3.Zero;

        public DateTimeOffset? RegisterDate { get; set; }
        public DateTimeOffset? LastLoginDate { get; set; }
        public DateTimeOffset? LockEnd { get; set; }
        public bool IsLocked { get; set; }
        //public eLanguage Language { get; set; }
    }
}
