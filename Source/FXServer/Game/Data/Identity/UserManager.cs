﻿using FXServer.Game.Data.Identity.Extensions;
using FXServer.Game.Data.Identity.Models;
using FXServer.Core.Data.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using FXServer.Core.Data;

namespace FXServer.Game.Data.Identity
{
    public class UserManager
    {
        public Account Model { get; protected set; }
        public UserManager(string userId) => Model = CoreSQL.Connect.Account.First(u => u.Id.Equals(userId, StringComparison.CurrentCultureIgnoreCase));
        public IQueryable<Account> Users => CoreSQL.Connect.Account;

        public ePlayerSpawnPoint[] GetSpawnPoints() => null;
        public ePlayerStyleComponents[] GetStyleComponents() => null;
        public DataAccountManager Profile => new DataAccountManager(Model); //Ready
        static public async Task<bool> Create()
        {
            using (CoreSQL SQL = new CoreSQL())
            {
                var _create = new Account
                {
                    Id = Guid.NewGuid().ToString()
                };
                SQL.Entry(_create);
                var _result = await SQL.SaveChangesAsync();
                return _result > 0 ? true : false;
            }
        }  //Current Developed
    }
}
