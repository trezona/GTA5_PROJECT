using FXServer.Core.Data;
using FXServer.Game.Data.Identity.Extensions;
using FXServer.Core.Data.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FXServer.Game.Data.Identity
{
    public class VehicleManager
    {
        public Vehicle Model { get; protected set; }
        public VehicleManager(string vehicleID) => Model = CoreSQL.Connect.Vehicles.First(u => u.Id.Equals(vehicleID, StringComparison.CurrentCultureIgnoreCase));
        public IQueryable<Vehicle> Vehicles => CoreSQL.Connect.Vehicles;

        public DataVehicleManager Profile => new DataVehicleManager(Model); //Ready
        static public async Task<bool> Create()
        {
            using (CoreSQL SQL = new CoreSQL())
            {
                var _create = new Vehicle
                {
                    Id = Guid.NewGuid().ToString()
                };
                SQL.Entry(_create);
                var _result = await SQL.SaveChangesAsync();
                return _result > 0 ? true : false;
            }
        }  //Current Developed
    }
}
