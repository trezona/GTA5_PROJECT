﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FXServer.Game.Data.Enums
{
    public enum eLanguage
    {
        Russian,
        English
    }
}
