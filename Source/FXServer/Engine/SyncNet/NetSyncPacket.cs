﻿using CitizenFX.Core;
using FXServer.Core.Components;
using System;

namespace FXServer.Engine.SyncNet
{
    public class NetSyncPacket : BaseScript
    {
        public NetSyncPacket()
        {
            EventHandlers["FX:SERVER:PACKET"] += new Action<Player, dynamic>(FX_SERVER_PACKET);
            EventHandlers["FX:SERVER:PACKET:PRIVATE"] += new Action<Player, dynamic, int>(FX_SERVER_PACKET_PRIVATE);
        }

        //PACKET
        private void FX_SERVER_PACKET([FromSource] Player fromPlayer, dynamic data) => onNetworkPacket(new Client(fromPlayer), data);
        public delegate void onNetworkPacketDelegate(Client client, dynamic data);
        private static void onNetworkPacketHandler(Client client, dynamic data)
        {
            Debug.WriteLine("[NETWORK][GET] FROM: {0}, PACKET: {1}", client.Name, data);
            TriggerClientEvent("FX:CLIENT:PACKET", data);
            Debug.WriteLine("[NETWORK][SEND] PACKET: {0}", data);
        }
        public static event onNetworkPacketDelegate onNetworkPacket = new onNetworkPacketDelegate(onNetworkPacketHandler);

        //PACKET PRIVATE
        private void FX_SERVER_PACKET_PRIVATE([FromSource] Player fromPlayer, dynamic data, int toPlayer) => onNetworkPacketPrivate(new Client(fromPlayer), new Client(toPlayer), data);
        public delegate void onNetworkPacketPrivateDelegate(Client client, Client toClient, dynamic data);
        private static void onNetworkPacketPrivateHandler(Client client, Client toClient, dynamic data)
        {
            Debug.WriteLine("[NETWORK][GET] FROM: {0}, PACKET: {1}, FOR: {2}", client.Name, data, toClient.Name);
            TriggerClientEvent(toClient.Player, "FX:CLIENT:PACKET:PRIVATE", data);
            Debug.WriteLine("[NETWORK][SEND] PACKET: {0}, FOR: {1}", data, toClient.Name);
        }
        public static event onNetworkPacketPrivateDelegate onNetworkPacketPrivate = new onNetworkPacketPrivateDelegate(onNetworkPacketPrivateHandler);

    }
}
