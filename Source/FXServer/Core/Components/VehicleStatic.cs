﻿using CitizenFX.Core;
using FXServer.Core.SyncNet;
using System;
using System.Collections.Generic;

namespace FXServer.Core.Components
{
    public class VehicleStatic
    {
        private Dictionary<int, int> Tuning = new Dictionary<int, int>();
        private Dictionary<string, dynamic> Car = new Dictionary<string, dynamic>();
        public VehicleStatic(uint Model, Vector3 position, float heading = 0)
        {
            Car["ID"] = Guid.NewGuid().ToString();
            Car["MODEL"] = Model;
            Car["HEADING"] = heading;
            Car["POS"] = new float[3] { position.X, position.Y, position.Z };
        }
        public VehicleStatic(uint Model, Vector3 position, Vector3 rotation)
        {
            Car["ID"] = Guid.NewGuid().ToString();
            Car["MODEL"] = Model;
            Car["HEADING"] = 0;
            Car["ROT"] = new float[3] { rotation.X, rotation.Y, rotation.Z };
            Car["POS"] = new float[3] { position.X, position.Y, position.Z };
        }
        public void SetTuning(int index, int variation)
        {
            Tuning[index] = variation;
            Car["STYLE"] = Tuning;
        }
        public void Complete() => Network.Send("vehicleStatic:add", Car);
    }
}
