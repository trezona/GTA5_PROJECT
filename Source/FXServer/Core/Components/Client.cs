﻿using CitizenFX.Core;
using FXServer.Core.SyncNet;

namespace FXServer.Core.Components
{
    public class Client
    {
        public Player Player { get; protected set; }
        public Client(Player Player)
        {
            this.Player = Player;
        }
        public Client(int handle) => Player = new PlayerList()[handle];
        public Client(string name) => Player = new PlayerList()[name];
        public string Name => Player?.Name;
        public int Handle => int.Parse(Player.Handle);
        public void Position(Vector3 position) => Network.Send("player:setPosition", new float[3] { position.X, position.Y, position.Z }, this);
        public void Rotation(Vector3 rotation) => Network.Send("player:setRotation", new float[3] { rotation.X, rotation.Y, rotation.Z }, this);
        public void Heading(float heading) => Network.Send("player:setHeading", heading, this);
        public void SetModel(uint model) => Network.Send("player:setModel", model, this);
    }
}
