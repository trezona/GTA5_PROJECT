﻿using FXServer.Core.Data.Models;
using MySql.Data.Entity;
using MySql.Data.MySqlClient;
using System.Data.Entity;

namespace FXServer.Core.Data
{
    class DbConnect
    {
        static public string Init => new MySqlConnectionStringBuilder
        {
            Server = "flygrand.ru",
            UserID = "gta5",
            Password = "gta5",
            Database = "gta5",
            Port = 3306
        }.ToString();
    }

    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class CoreSQL : DbContext
    {
        static public CoreSQL Connect { get; } = new CoreSQL();
        public CoreSQL() : base(DbConnect.Init) { }

        //Tables
        public DbSet<Account> Account { get; set; }
        public DbSet<DataAccount> DataAccount { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<DataVehicle> DataVehicle { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Account>().ToTable("Accounts").HasKey(k => k.Id);
            modelBuilder.Entity<DataAccount>().ToTable("DataAccount").HasKey(k => k.Id);
            modelBuilder.Entity<Vehicle>().ToTable("Vehicles").HasKey(k => k.Id);
            modelBuilder.Entity<DataVehicle>().ToTable("DataVehicle").HasKey(k => k.Id);
        }
    }
}
