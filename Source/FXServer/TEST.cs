﻿using CitizenFX.Core;
using FXServer.Core.Components;
using FXServer.Core.SyncNet;
using System;

namespace FXServer
{
    public class TEST : BaseScript
    {
        public TEST()
        {
            EventHandlers["playerConnecting"] += new Action<Player>(Connected);
        }

        private void Connected([FromSource] Player obj)
        {
            Debug.WriteLine("Connected {0} id:{1}", obj.Name, obj.Handle);
        }
    }
}
