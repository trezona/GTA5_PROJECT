﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;

namespace Server_Session
{
    class Init : BaseScript
    {
        public Init()
        {
            EventHandlers["hostingSession"] += new Action<Player>(onHostingSession);
            EventHandlers["hostedSession"] += new Action<Player>(onHostedSession);
        }

        Player currentHosting = null;
        List<Player> hostReleaseCallbacks = new List<Player>();
        private void onHostingSession([FromSource] Player obj)
        {
            Function.Call(Hash.ENABLE_ENHANCED_HOST_SUPPORT, true);
            if (currentHosting != null)
            {
                TriggerClientEvent(obj, "sessionHostResult", "wait");
                hostReleaseCallbacks.Add(obj);
                return;
            }
            var getHostId = Function.Call<dynamic>(Hash.GET_HOST_ID);
            Debug.Write("[WARN] " + getHostId);
            if (!string.IsNullOrWhiteSpace(getHostId))
            {
                if(Function.Call<int>(Hash.GET_PLAYER_LAST_MSG, getHostId) < 1000)
                {
                    TriggerClientEvent(obj, "sessionHostResult", "conflict");
                    return;
                }
            }

            hostReleaseCallbacks.Clear();
            currentHosting = obj;
            TriggerClientEvent(obj, "sessionHostResult", "go");

            Delay(5000);
            if (currentHosting == null) return;
            currentHosting = null;

            foreach(var raw in hostReleaseCallbacks)
            {
                TriggerClientEvent(raw, "sessionHostResult", "free");
            }
        }

        private void onHostedSession([FromSource] Player obj)
        {
            if(currentHosting != obj)
            {
                Debug.Write($"[WARN] Player {obj.Name} Dropped");
                return;
            }
            foreach (var raw in hostReleaseCallbacks)
            {
                TriggerClientEvent(raw, "sessionHostResult", "free");
            }
            currentHosting = null;
        }

    }
}
