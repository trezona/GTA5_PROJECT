﻿using CitizenFX.Core;
using LibSharp.CSharpCore;

namespace _009S_vehicles
{
    //Строго NATIVE !!
    public class VehicleModel
    {
        public int ID { get; set; } = -1;
        public int Handle { get; set; } = 0;
        public ulong Model { get; set; } = 3950024287; //Blista
        public float[] Position { get; set; } = new float[3];
        public float[] Rotation { get; set; } = new float[3];
    }

    public class CreateVehicle
    {
        protected VehicleModel Vehicle = new VehicleModel();
        public CreateVehicle(Vector3 position, Vector3 rotation)
        {
            Position = position;
            Rotation = rotation;
        }
        public CreateVehicle(ulong model, Vector3 position, Vector3 rotation) : this(position, rotation) => Vehicle.Model = model;
        public VehicleModel VehicleObject => Vehicle;
        public Vector3 Position
        {
            get => new Vector3(Vehicle.Position[0], Vehicle.Position[1], Vehicle.Position[2]);
            set => Vehicle.Position = new float[3] { value.X, value.Y, value.Z };
        }
        public Vector3 Rotation
        {
            get => new Vector3(Vehicle.Rotation[0], Vehicle.Rotation[1], Vehicle.Rotation[2]);
            set => Vehicle.Rotation = new float[3] { value.X, value.Y, value.Z };
        }
        public void Save() => Network.Local("vehicleStatic:serverSave", VehicleObject);
    }
}
