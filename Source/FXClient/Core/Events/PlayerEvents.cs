﻿using System.Threading.Tasks;
using CitizenFX.Core;
using System;
using CitizenFX.Core.Native;

namespace FXClient.Core.Events
{
    public class PlayerEvents : BaseScript
    {
        public delegate void PlayerSpawnedConnected();
        public PlayerEvents()
        {
            EventHandlers["playerSpawned"] += new Action(() => OnSpawnConnected());
            Tick += DeathEvents;
            Tick += VehicleEvents;
        }

        public static event PlayerSpawnedConnected OnSpawnConnected = new PlayerSpawnedConnected(OnSpawnConnectedHandler);
        protected static void OnSpawnConnectedHandler() { }

        #region VehicleEvents

        public delegate void EnteringVehicleDelegate(Vehicle vehicle, VehicleSeat seat);
        public delegate void EnteringAbortedDelegate(Vehicle vehicle, VehicleSeat seat);
        public delegate void EnteredVehicleDelegate(Vehicle vehicle, VehicleSeat seat);
        public delegate void LeftVehicleDelegate(Vehicle vehicle, VehicleSeat seat);
        public delegate void LeftVehicleJumpDelegate(Vehicle vehicle, VehicleSeat seat);

        private static void onEnteringVehicleHandler(Vehicle vehicle, VehicleSeat seat) {  }
        private static void onEnteringAbortedHandler(Vehicle vehicle, VehicleSeat seat) { }
        private static void onEnteredVehicleHandler(Vehicle vehicle, VehicleSeat seat) { }
        private static void onLeftVehicleHandler(Vehicle vehicle, VehicleSeat seat) {  }
        private static void onLeftVehicleJumpHandler(Vehicle vehicle, VehicleSeat seat) { }

        public static event EnteringVehicleDelegate onEnteringVehicle = new EnteringVehicleDelegate(onEnteringVehicleHandler);
        public static event EnteringAbortedDelegate onEnteringAborted = new EnteringAbortedDelegate(onEnteringAbortedHandler);
        public static event EnteredVehicleDelegate onEnteredVehicle = new EnteredVehicleDelegate(onEnteredVehicleHandler);
        public static event LeftVehicleDelegate onLeftVehicle = new LeftVehicleDelegate(onLeftVehicleHandler);
        public static event LeftVehicleJumpDelegate onLeftVehicleJump = new LeftVehicleJumpDelegate(onLeftVehicleJumpHandler);


        bool isInVehicle = false;
        bool isEnteringVehicle = false;
        Vehicle currentVehicle = null;
        int currentSeat = -3;
        private Task VehicleEvents()
        {

            Ped ped = Game.PlayerPed;
            if (!isInVehicle && ped.IsAlive)
            {
                Vehicle vehicleObject = ped.VehicleTryingToEnter;
                if (vehicleObject != null && !isEnteringVehicle)
                {
                    currentSeat = Function.Call<int>(Hash.GET_SEAT_PED_IS_TRYING_TO_ENTER, ped);
                    onEnteringVehicle(currentVehicle = vehicleObject, (VehicleSeat)currentSeat);
                    isEnteringVehicle = true;
                }
                else if (vehicleObject == null && !ped.IsInVehicle() && isEnteringVehicle)
                {
                    onEnteringAborted(currentVehicle, (VehicleSeat)currentSeat);
                    isEnteringVehicle = false;
                    currentVehicle = null;
                    currentSeat = -3;
                }
                else if (ped.IsInVehicle())
                {
                    isEnteringVehicle = false;
                    isInVehicle = true;
                    currentVehicle = ped.CurrentVehicle;
                    currentSeat = (int)ped.SeatIndex;
                    onEnteredVehicle(currentVehicle, (VehicleSeat)currentSeat);
                }
            }
            else if (isInVehicle)
            {
                if (!ped.IsInVehicle() || ped.IsDead)
                {
                    if (currentVehicle.Speed > 1) onLeftVehicleJump(currentVehicle, (VehicleSeat)currentSeat);
                    else onLeftVehicle(currentVehicle, (VehicleSeat)currentSeat);
                    isInVehicle = false;
                    currentSeat = -3;
                    currentVehicle = null;
                }
            }
            return Task.FromResult(true);
        }

        #endregion VehicleEvents

        #region DeathEvents
        public delegate void PlayerDied();
        public delegate void PlayerKilled(int killerHandle);
        public delegate void PlayerWasted();

        private static void onPlayerDiedHandler() { TriggerServerEvent("event:onPlayerDied"); }
        private static void onPlayerKilledHandler(int killerHandle) { TriggerServerEvent("event:onPlayerKilled", killerHandle); }
        private static void onPlayerWastedHandler() { TriggerServerEvent("event:onPlayerWasted"); }

        public static event PlayerDied onDied = new PlayerDied(onPlayerDiedHandler);
        public static event PlayerKilled OnKilled = new PlayerKilled(onPlayerKilledHandler);
        public static event PlayerWasted onWasted = new PlayerWasted(onPlayerWastedHandler);


        bool isDied = false;
        bool hasBeenDead = false;
        int diedAt = 0;
        private Task DeathEvents()
        {
           Ped ped = Game.PlayerPed;
            if (ped.IsDead && !isDied)
            {
                isDied = true;
                if (diedAt == 0) diedAt = Game.GameTime;
                Entity killer = ped.GetKiller();
                if (killer != null)
                {
                    if (ped.Handle == killer.Handle)
                    {
                        onDied();
                        hasBeenDead = true;
                    }
                    else if (killer.Handle >= 0)
                    {
                        OnKilled(killer.Handle);
                        hasBeenDead = true;
                    }
                }
            }
            else if (ped.IsAlive && hasBeenDead)
            {
                isDied = false;
                diedAt = 0;
            }
            if (!hasBeenDead && diedAt > 0)
            {
                onWasted();
                hasBeenDead = true;
            }
            else if (hasBeenDead && diedAt <= 0) hasBeenDead = false; return Task.FromResult(true);
        }
        #endregion DeathEvents
    }
}
