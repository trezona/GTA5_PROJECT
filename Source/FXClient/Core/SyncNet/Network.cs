﻿using CitizenFX.Core;
using System;

namespace FXClient.Core.SyncNet
{
    public class Network : BaseScript
    {
        public Network()
        {
            //Trigger
            EventHandlers["FX:CLIENT:TRIGGER"] += new Action<string>((trigger) => onNetworkTrigger(trigger));
            EventHandlers["FX:CLIENT:TRIGGER:PRIVATE"] += new Action<string>((trigger) => onNetworkTriggerPrivate(trigger));

            //Data
            EventHandlers["FX:CLIENT:DATA"] += new Action<string, dynamic>((trigger, data) => onNetworkData(trigger, data));
            EventHandlers["FX:CLIENT:DATA:PRIVATE"] += new Action<string, dynamic>((trigger, data) => onNetworkDataPrivate(trigger, data));
        }

        //TRIGGER
        public delegate void onNetworkTriggerDelegate(string triggerName);
        static private void onNetworkTriggerHandler(string triggerName) { }
        static public event onNetworkTriggerDelegate onNetworkTrigger = new onNetworkTriggerDelegate(onNetworkTriggerHandler);

        //TRIGGER PRIVATE
        public delegate void onNetworkTriggerPrivateDelegate(string triggerName);
        static private void onNetworkTriggerPrivateHandler(string triggerName) { }
        static public event onNetworkTriggerPrivateDelegate onNetworkTriggerPrivate = new onNetworkTriggerPrivateDelegate(onNetworkTriggerPrivateHandler);

        //DATA
        public delegate void onNetworkDataDelegate(string triggerName, dynamic data);
        static private void onNetworkDataHandler(string triggerName, dynamic data) { }
        static public event onNetworkDataDelegate onNetworkData = new onNetworkDataDelegate(onNetworkDataHandler);

        //DATA PRIVATE
        public delegate void onNetworkDataPrivateDelegate(string triggerName, dynamic data);
        static private void onNetworkDataPrivateHandler(string triggerName, dynamic data) { }
        static public event onNetworkDataPrivateDelegate onNetworkDataPrivate = new onNetworkDataPrivateDelegate(onNetworkDataPrivateHandler);

        //CLIENT-SIDE => API

        //Trigger
        static public void Send(string triggerName)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}", triggerName);
            TriggerServerEvent("FX:SERVER:TRIGGER", triggerName);
        }
        static public void Send(string triggerName, Player player)
        {
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FOR: {1}", triggerName, player.Name);
            TriggerServerEvent("FX:SERVER:TRIGGER:PRIVATE", triggerName, player.ServerId);
        }

        //Data
        static public void Send<T>(string triggerName, T data)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
            TriggerServerEvent("FX:SERVER:DATA", triggerName, data);
        }
        static public void Send<T>(string triggerName, T data, Player player)
        {
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
            TriggerServerEvent("FX:SERVER:DATA:PRIVATE", triggerName, data, player.ServerId);
        }

        //Packet
        static public void Send(Action data)
        {
            Debug.WriteLine("[NETWORK][SEND] PACKET: {0}", data);
            TriggerServerEvent("FX:SERVER:PACKET", data);
        }
        static public void Send(Action data, Player player)
        {
            Debug.WriteLine("[NETWORK][SEND] PACKET: {0}, FOR: {1}", data, player.Name);
            TriggerServerEvent("FX:SERVER:PACKET:PRIVATE", data, player.ServerId);
        }
    }
}
