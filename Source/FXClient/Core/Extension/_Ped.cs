﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;

namespace FXClient.Core.Extension
{
    public static class _Ped
    {
        static public void SetCanAttackFriendly(this Ped ped, bool enable) => Function.Call(Hash.SET_CAN_ATTACK_FRIENDLY, ped.Handle, enable);
        static public void SetPedCanBeDraggedOut(this Ped ped, bool enable) => Function.Call(Hash.SET_PED_CAN_BE_DRAGGED_OUT, ped.Handle, enable);
        static public void SetDriveCruiseSpeed(this Ped ped, float speed) => Function.Call(Hash.SET_DRIVE_TASK_CRUISE_SPEED, ped.Handle, speed);
        static public void SetDriveMaxCruiseSpeed(this Ped ped, float speed) => Function.Call(Hash.SET_DRIVE_TASK_MAX_CRUISE_SPEED, ped.Handle, speed);
        static public VehicleSeat IsSeatIndex(this Ped ped)
        {
            Vehicle veh = ped.CurrentVehicle;
            foreach (var i in Enum.GetValues(typeof(VehicleSeat)))
            {
                if (veh.IsSeatFree((VehicleSeat)i)) continue;
                if (veh.GetPedOnSeat((VehicleSeat)i) == Game.PlayerPed) return (VehicleSeat)i;
            }
            return VehicleSeat.None;
        }
    }
}
