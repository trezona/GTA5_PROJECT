﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;

namespace FXClient.Core.Components
{
    public class Camera : PoolObject, IEquatable<Camera>, ISpatial
    {
        internal static readonly string[] _shakeNames = {
            "HAND_SHAKE",
            "SMALL_EXPLOSION_SHAKE",
            "MEDIUM_EXPLOSION_SHAKE",
            "LARGE_EXPLOSION_SHAKE",
            "JOLT_SHAKE",
            "VIBRATE_SHAKE",
            "ROAD_VIBRATION_SHAKE",
            "DRUNK_SHAKE",
            "SKY_DIVING_SHAKE",
            "FAMILY5_DRUG_TRIP_SHAKE",
            "DEATH_FAIL_IN_EFFECT_SHAKE"
        };
        float _nearDepthOfField, _depthOfFieldStrength, _motionBlurStrength, _shakeAmplitude;
        public Camera(int handle) : base(handle) { }
        public Camera(Vector3 position, Vector3 rotation, float fov) : base(Function.Call<int>(Hash.CREATE_CAM_WITH_PARAMS, "DEFAULT_SCRIPTED_CAMERA", position.X, position.Y, position.Z, rotation.X, rotation.Y, rotation.Z, fov, 1, 2)) { }
        public Vector3 Position { get => Function.Call<Vector3>(Hash.GET_CAM_COORD, Handle); set => Function.Call(Hash.SET_CAM_COORD, Handle, value.X, value.Y, value.Z); }
        public Vector3 Rotation { get => Function.Call<Vector3>(Hash.GET_CAM_ROT, Handle, 2); set => Function.Call(Hash.SET_CAM_ROT, Handle, value.X, value.Y, value.Z, 2); }
        public bool Equals(Camera other) => !ReferenceEquals(other, null) && Handle == other.Handle;
        public bool IsActive { get => Function.Call<bool>(Hash.IS_CAM_ACTIVE, Handle); set => Function.Call(Hash.SET_CAM_ACTIVE, Handle, value); }
        public Vector3 Direction
        {
            get => Forward;
            set
            {
                value.Normalize();
                Vector3 vector1 = new Vector3(value.X, value.Y, 0f);
                Vector3 vector2 = new Vector3(value.Z, vector1.Length(), 0f);
                Vector3 vector3 = Vector3.Normalize(vector2);
                Rotation = new Vector3((float)(Math.Atan2(vector3.X, vector3.Y) * 57.295779513082323), Rotation.Y, (float)(Math.Atan2(value.X, value.Y) * -57.295779513082323));
            }
        }
        public float FieldOfView { get => Function.Call<float>(Hash.GET_CAM_FOV, Handle); set => Function.Call(Hash.SET_CAM_FOV, Handle, value); }
        public float NearClip { get => Function.Call<float>(Hash.GET_CAM_NEAR_CLIP, Handle); set => Function.Call(Hash.SET_CAM_NEAR_CLIP, Handle, value); }
        public float FarClip { get => Function.Call<float>(Hash.GET_CAM_FAR_CLIP, Handle); set => Function.Call(Hash.SET_CAM_FAR_CLIP, Handle, value); }
        public float NearDepthOfField
        {
            get => _nearDepthOfField; set
            {
                _nearDepthOfField = value;
                Function.Call(Hash.SET_CAM_NEAR_DOF, Handle, value);
            }
        }
        public float FarDepthOfField { get => Function.Call<float>(Hash.GET_CAM_FAR_DOF, Handle); set => Function.Call(Hash.SET_CAM_FAR_DOF, Handle, value); }
        public float DepthOfFieldStrength
        {
            get => _depthOfFieldStrength; set
            {
                _depthOfFieldStrength = value;
                Function.Call(Hash.SET_CAM_DOF_STRENGTH, Handle, value);
            }
        }
        public float MotionBlurStrength
        {
            get => _motionBlurStrength;
            set
            {
                _motionBlurStrength = value;
                Function.Call(Hash.SET_CAM_MOTION_BLUR_STRENGTH, Handle, value);
            }
        }
        public void Shake(CameraShake shakeType, float amplitude) => Function.Call(Hash.SHAKE_CAM, Handle, _shakeNames[(int)shakeType], amplitude);
        public void StopShaking() => Function.Call(Hash.STOP_CAM_SHAKING, Handle, true);
        public bool IsShaking => Function.Call<bool>(Hash.IS_CAM_SHAKING, Handle);
        public bool IsRendering { get => DefaultCamera.RenderingCamera.Handle == Handle; set { DefaultCamera.RenderingCamera = value ? this : DefaultCamera.RenderingCamera; } }
        public float ShakeAmplitude
        {
            get => _shakeAmplitude;
            set
            {
                _shakeAmplitude = value;
                Function.Call(Hash.SET_CAM_SHAKE_AMPLITUDE, Handle, value);
            }
        }
        public void PointAt(CitizenFX.Core.Entity target, Vector3 offset = default(Vector3)) => Function.Call(Hash.POINT_CAM_AT_ENTITY, Handle, target.Handle, offset.X, offset.Y, offset.Z, true);
        public void PointAt(PedBone target, Vector3 offset = default(Vector3)) => Function.Call(Hash.POINT_CAM_AT_PED_BONE, Handle, target.Owner.Handle, target, offset.X, offset.Y, offset.Z, true);
        public void PointAt(Vector3 target) => Function.Call(Hash.POINT_CAM_AT_COORD, Handle, target.X, target.Y, target.Z);
        public void StopPointing() => Function.Call(Hash.STOP_CAM_POINTING, Handle);
        public void InterpTo(Camera to, int duration, bool easePosition, bool easeRotation) => Function.Call(Hash.SET_CAM_ACTIVE_WITH_INTERP, to.Handle, Handle, duration, easePosition, easeRotation);
        public bool IsInterpolating => Function.Call<bool>(Hash.IS_CAM_INTERPOLATING, Handle);
        public void AttachTo(CitizenFX.Core.Entity entity, Vector3 offset) => Function.Call(Hash.ATTACH_CAM_TO_ENTITY, Handle, entity.Handle, offset.X, offset.Y, offset.Z, true);
        public void AttachTo(PedBone pedBone, Vector3 offset) => Function.Call(Hash.ATTACH_CAM_TO_PED_BONE, Handle, pedBone.Owner.Handle, pedBone, offset.X, offset.Y, offset.Z, true);
        public void Detach() => Function.Call(Hash.DETACH_CAM, Handle);
        public override void Delete() => Function.Call(Hash.DESTROY_CAM, Handle, 0);
        public Vector3 Forward => GameMath.RotationToDirection(Rotation);
        public Vector3 Backward => -Forward;
        public Vector3 Right => Vector3.Cross(Forward, Vector3.UnitZ);
        public Vector3 Left => -Right;
        public override bool Exists() => Function.Call<bool>(Hash.DOES_CAM_EXIST, Handle);
        public static bool Exists(Camera camera) => !ReferenceEquals(camera, null) && camera.Exists();
    }
}
