﻿using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace FXClient.Core.Extension
{
    public static class _Vehicle
    {
        static public void EnableRadio(this Vehicle vehicle, bool toogle) => Function.Call(Hash.SET_VEHICLE_RADIO_ENABLED, vehicle.Handle, toogle);
    }
}
