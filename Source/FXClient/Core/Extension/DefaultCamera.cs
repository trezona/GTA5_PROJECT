﻿using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace FXClient.Core.Components
{
    public static class DefaultCamera
    {
        public static float _shakeAmplitude;
        public static Vector3 Position => Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_COORD);
        public static Vector3 Rotation => Function.Call<Vector3>(Hash.GET_GAMEPLAY_CAM_ROT, 2);
        public static Vector3 Forward => GameMath.RotationToDirection(Rotation);
        public static Vector3 Backward => -Forward;
        public static Vector3 Right => Vector3.Cross(Forward, Vector3.UnitZ);
        public static Vector3 Left => -Right;
        public static float RelativePitch { get => Function.Call<float>(Hash.GET_GAMEPLAY_CAM_RELATIVE_PITCH); set => Function.Call(Hash.SET_GAMEPLAY_CAM_RELATIVE_PITCH, value); }
        public static float RelativeHeading { get => Function.Call<float>(Hash.GET_GAMEPLAY_CAM_RELATIVE_HEADING); set => Function.Call(Hash.SET_GAMEPLAY_CAM_RELATIVE_HEADING, value); }
        public static void ClampYaw(float min, float max) => Function.Call(Hash._CLAMP_GAMEPLAY_CAM_YAW, min, max);
        public static void ClampPitch(float min, float max) => Function.Call(Hash._CLAMP_GAMEPLAY_CAM_PITCH, min, max);
        public static float Zoom => Function.Call<float>(Hash._GET_GAMEPLAY_CAM_ZOOM);
        public static float FieldOfView => Function.Call<float>(Hash.GET_GAMEPLAY_CAM_FOV);
        public static bool IsRendering { get => Function.Call<bool>(Hash.IS_GAMEPLAY_CAM_RENDERING); set { World.RenderingCamera = value ? null : World.RenderingCamera; } }
        public static bool IsAimCamActive => Function.Call<bool>(Hash.IS_AIM_CAM_ACTIVE);
        public static bool IsFirstPersonAimCamActive => Function.Call<bool>(Hash.IS_FIRST_PERSON_AIM_CAM_ACTIVE);
        public static bool IsLookingBehind => Function.Call<bool>(Hash.IS_GAMEPLAY_CAM_LOOKING_BEHIND);
        public static void Shake(CameraShake shakeType, float amplitude) => Function.Call(Hash.SHAKE_GAMEPLAY_CAM, Camera._shakeNames[(int)shakeType], amplitude);
        public static void StopShaking() => Function.Call(Hash.STOP_GAMEPLAY_CAM_SHAKING, true);
        public static bool IsShaking => Function.Call<bool>(Hash.IS_GAMEPLAY_CAM_SHAKING);
        public static float ShakeAmplitude
        {
            get => _shakeAmplitude;
            set
            {
                _shakeAmplitude = value;
                Function.Call(Hash.SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE, value);
            }
        }
        public static Camera RenderingCamera
        {
            get => new Camera(Function.Call<int>(Hash.GET_RENDERING_CAM));
            set
            {
                if (value == null) Function.Call(Hash.RENDER_SCRIPT_CAMS, false, 0, 3000, 1, 0);
                else
                {
                    value.IsActive = true;
                    Function.Call(Hash.RENDER_SCRIPT_CAMS, true, 0, 3000, 1, 0);
                }
            }
        }
        public static void DestroyAllCameras() => Function.Call(Hash.DESTROY_ALL_CAMS, 0);
    }
}
