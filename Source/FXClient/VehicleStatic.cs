﻿using CitizenFX.Core;
using FXClient.Core.SyncNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace FXServer.Core.Components
{
    public class VehicleStatic
    {
        Dictionary<int, int> Tuning = new Dictionary<int, int>();
        Dictionary<string, dynamic> Car = new Dictionary<string, dynamic>();
        public VehicleStatic()
        {
            Car["ID"] = Guid.NewGuid().ToString();
            Car["HEADING"] = 0;
            Car["STYLE"] = Tuning.ToArray();
            Car["COLOR1"] = new byte[3] { 0, 0, 0 };
            Car["COLOR2"] = new int[3] { 0, 0, 0 };
        }
        public VehicleStatic(uint Model, Vector3 position, float heading = 0) : this()
        {
            Car["MODEL"] = Model;
            Car["HEADING"] = heading;
            Car["POS"] = new float[3] { position.X, position.Y, position.Z };
        }
        public VehicleStatic(uint Model, Vector3 position, Vector3 rotation) : this()
        {
            Car["MODEL"] = Model;
            Car["ROT"] = new float[3] { rotation.X, rotation.Y, rotation.Z };
            Car["POS"] = new float[3] { position.X, position.Y, position.Z };
        }

        public Color Color1 { set { Car["COLOR1"] = new int[3] { value.R, value.G, value.B }; } }
        public Color Color2 { set { Car["COLOR2"] = new int[3] { value.R, value.G, value.B }; } }
        public void SetTuning(int index, int variation)
        {
            Tuning[index] = variation;
            Car["STYLE"] = Tuning.ToArray();
        }

        public void Save()
        {
            Car["HANDLE"] = 0;
            Network.Send("vehicleStatic:add", Car.ToArray(), null);
        }
    }
}
