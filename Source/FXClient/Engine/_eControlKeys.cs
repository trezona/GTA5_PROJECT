﻿using CitizenFX.Core;

namespace FXClient.Core.Config.List
{
    //Default keys
    public enum _eControlKeys
    {
        Q = Control.VehicleRadioWheel,
        W = Control.MoveUpOnly,
        E = Control.VehicleHorn,
        A = Control.MoveLeftOnly,
        D = Control.MoveRightOnly,
        S = Control.MoveDownOnly,
        Z = Control.MultiplayerInfo,
    }
}
