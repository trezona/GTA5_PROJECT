﻿using CitizenFX.Core;

namespace LibSharp.CSharpCore.Extensions
{
    static public class _Entity
    {
        public static Vector3 Forward(this Entity ent) => GameMath.RotationToDirection(ent.Rotation);
        public static Vector3 Backward(this Entity ent) => -Forward(ent);
        public static Vector3 Right(this Entity ent) => Vector3.Cross(Forward(ent), Vector3.UnitZ);
        public static Vector3 Left(this Entity ent) => -Right(ent);
    }
}
