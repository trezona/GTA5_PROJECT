﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using System.Threading.Tasks;

namespace Speedometer
{
    public class Init : Core
    {
        bool keep = true;
        public override void ResourceStart()
        {
            onEnteredVehicle += Init_onEnteredVehicle;
            onLeftVehicle += Init_onLeftVehicle;
        }

        private void Init_onLeftVehicle(Vehicle vehicle, VehicleSeat seat) => keep = true;
        private void Init_onEnteredVehicle(Vehicle vehicle, VehicleSeat seat) => keep = false;

        public override Task Update()
        {
            if (keep)
            {
                Function.Call(Hash.SEND_NUI_MESSAGE, "{ \"speeding\" : false }");
                return base.Update();
            }
            var vehicleObject = Game.PlayerPed.CurrentVehicle;
            if (vehicleObject != null) Function.Call(Hash.SEND_NUI_MESSAGE, "{ " + $" \"speeding\" : true, \"speed\" : {vehicleObject.Speed * 3.6}, \"rpm\" : {vehicleObject.CurrentRPM}, \"fuel\" : {vehicleObject.FuelLevel}" + "}");
            else
            {
                Function.Call(Hash.SEND_NUI_MESSAGE, "{ \"speeding\" : false }");
                keep = true;
            }
            return base.Update();
        }
    }
}
