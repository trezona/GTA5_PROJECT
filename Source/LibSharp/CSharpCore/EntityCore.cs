﻿using CitizenFX.Core;
using static CitizenFX.Core.Native.API;
using System;
using CitizenFX.Core.Native;
using System.Linq;

namespace LibSharp.CSharpCore
{
    public class EntityCore : PoolObject, IEquatable<EntityCore>, ISpatial
    {
        private _EntityBoneCollection _bones;
        protected EntityCore(int handle) : base(handle) { }
        public Vector3 Position { get => GetEntityCoords(Handle, false); set => SetEntityCoords(Handle, value.X, value.Y, value.Z, false, false, false, false); }
        public void PositionNoOffset(Vector3 position) => SetEntityCoordsNoOffset(Handle, position.X, position.Y, position.Z, true, true, true);
        public Vector3 Rotation { get => GetEntityRotation(Handle, 2); set => SetEntityRotation(Handle, value.X, value.Y, value.Z, 2, true); }
        public override void Delete()
        {
            SetEntityAsMissionEntity(Handle, false, true);
            int handle = 0;
            DeleteEntity(ref handle);
            Handle = handle;
        }
        public void MarkAsNoLongerNeeded()
        {
            SetEntityAsMissionEntity(Handle, false, true);
            int handle = 0;
            SetEntityAsNoLongerNeeded(ref handle);
            Handle = handle;
        }
        public bool Equals(EntityCore other) => !ReferenceEquals(other, null) && Handle == other.Handle;
        public override bool Equals(object obj) => !ReferenceEquals(obj, null) && obj.GetType() == GetType() && Equals((EntityCore)obj);
        public override bool Exists() => DoesEntityExist(Handle);
        public static bool Exists(EntityCore entity) => !ReferenceEquals(entity, null) && entity.Exists();
        public override int GetHashCode() => Handle.GetHashCode();
        public static EntityCore FromHandle(int handle)
        {
            switch (GetEntityType(handle))
            {
                case 1:
                    break;//Ped
                case 2:
                    return new VehicleCore(handle);
                case 3:
                    break;//Prop
            }
            return null;
        }
        public void RemoveAllParticleEffects() => RemoveParticleFxFromEntity(Handle);
        public void ApplyForceRelative(Vector3 direction, Vector3 rotation = default(Vector3), ForceType forceType = ForceType.MaxForceRot2) => ApplyForceToEntity(Handle, (int)forceType, direction.X, direction.Y, direction.Z, rotation.X, rotation.Y, rotation.Z, 0, true, true, true, false, true);
        public void ApplyForce(Vector3 direction, Vector3 rotation = default(Vector3), ForceType forceType = ForceType.MaxForceRot2) => ApplyForceToEntity(Handle, (int)forceType, direction.X, direction.Y, direction.Z, rotation.X, rotation.Y, rotation.Z, 0, false, true, true, false, true);
        public EntityCore GetEntityAttachedTo() => FromHandle(API.GetEntityAttachedTo(Handle));
        public bool IsAttachedTo(EntityCore entity) => IsEntityAttachedToEntity(Handle, entity.Handle);
        public bool IsAttached() => IsEntityAttached(Handle);
        public void Detach() => DetachEntity(Handle, true, true);
        public void AttachTo(EntityBone entityBone, Vector3 position = default(Vector3), Vector3 rotation = default(Vector3)) => AttachEntityToEntity(Handle, entityBone.Owner.Handle, entityBone, position.X, position.Y, position.Z, rotation.X, rotation.Y, rotation.Z, false, false, false, false, 2, true);
        public void AttachTo(EntityCore entity, Vector3 position = default(Vector3), Vector3 rotation = default(Vector3)) => AttachEntityToEntity(Handle, entity.Handle, -1, position.X, position.Y, position.Z, rotation.X, rotation.Y, rotation.Z, false, false, false, false, 2, true);
        public Blip[] AttachedBlips() => World.GetAllBlips().Where(x => GetBlipInfoIdEntityIndex(x.Handle) == Handle).ToArray();
        public Blip AttachedBlip
        {
            get
            {
                int handle = GetBlipFromEntity(Handle);
                if (DoesBlipExist(handle)) return new Blip(handle);
                return null;
            }
        }
        public Blip AttachBlip() => new Blip(AddBlipForEntity(Handle));
        public _EntityBoneCollection Bones
        {
            get
            {
                if (ReferenceEquals(_bones, null)) _bones = new _EntityBoneCollection(this);
                return _bones;
            }
        }
        public Vector3 GetPositionOffset(Vector3 worldCoords) => GetOffsetFromEntityGivenWorldCoords(Handle, worldCoords.X, worldCoords.Y, worldCoords.Z);
        public Vector3 GetOffsetPosition(Vector3 offset) => GetOffsetFromEntityInWorldCoords(Handle, offset.X, offset.Y, offset.Z);
        public bool IsTouching(EntityCore entity) => IsEntityTouchingEntity(Handle, entity.Handle);
        public bool IsTouching(Model model) => IsEntityTouchingModel(Handle, (uint)model.Hash);
        public int Health
        {
            get => GetEntityHealth(Handle);
            set => SetEntityHealth(Handle, value);
        }
        public int MaxHealth
        {
            get => GetEntityMaxHealth(Handle);
            set => SetEntityMaxHealth(Handle, value);
        }
        public bool IsDead => IsEntityDead(Handle);
        public bool IsAlive => !IsDead;
        public Model Model => new Model(GetEntityModel(Handle));
        public Quaternion Quaternion
        {
            get
            {
                float x = 0, y = 0, z = 0, w = 0;
                GetEntityQuaternion(Handle, ref x, ref y, ref z, ref w);
                return new Quaternion(x, y, z, w);
            }
            set => SetEntityQuaternion(Handle, value.X, value.Y, value.Z, value.W);
        }
        public float Heading
        {
            get => GetEntityHeading(Handle);
            set => SetEntityHeading(Handle, value);
        }
        public Vector3 Up => Matrix.Up;
        public Vector3 Right => Matrix.Right;
        public Vector3 Forward => Matrix.Forward;
        public Matrix Matrix
        {
            get
            {
                Vector3 right = Vector3.Zero;
                Vector3 forward = Vector3.Zero;
                Vector3 up = Vector3.Zero;
                Vector3 at = Vector3.Zero;
                GetEntityMatrix(Handle, ref right, ref forward, ref up, ref at);
                return new Matrix(
                    right.X, right.Y, right.Z, 0.0f,
                    forward.X, forward.Y, forward.Z, 0.0f,
                    up.X, up.Y, up.Z, 0.0f,
                    at.X, at.Y, at.Z, 1.0f
                );
            }
        }
        public void PositionFrozen(bool toggle) => FreezeEntityPosition(Handle, toggle);
        public Vector3 Velocity
        {
            get => GetEntityVelocity(Handle);
            set => SetEntityVelocity(Handle, value.X, value.Y, value.Z);
        }
        public Vector3 RotationVelocity() => GetEntityRotationVelocity(Handle);
        public void MaxSpeed(float maxSpeed) => SetEntityMaxSpeed(Handle, maxSpeed);
        public void HasGravity(bool toggle) => SetEntityHasGravity(Handle, toggle);
        public float HeightAboveGround() => GetEntityHeightAboveGround(Handle);
        public float SubmersionLevel() => GetEntitySubmergedLevel(Handle);
        public int LodDistance
        {
            get => GetEntityLodDist(Handle);
            set => SetEntityLodDist(Handle, value);
        }
        public bool IsVisible
        {
            get => IsEntityVisible(Handle);
            set => SetEntityVisible(Handle, value, false);
        }
        public bool IsOccluded() => IsEntityOccluded(Handle);
        public bool IsOnScreen() => IsEntityOnScreen(Handle);
        public bool IsUpright() => IsEntityUpright(Handle, 0);
        public bool IsUpsideDown() => IsEntityUpsidedown(Handle);
        public bool IsInAir() => IsEntityInAir(Handle);
        public bool IsInWater() => IsEntityInWater(Handle);
        public bool IsPersistent
        {
            get => IsEntityAMissionEntity(Handle);
            set
            {
                if (value) SetEntityAsMissionEntity(Handle, true, false);
                else MarkAsNoLongerNeeded();
            }
        }
        public bool IsOnFire() => IsEntityOnFire(Handle);
        public void Invincible(bool toggle) => SetEntityInvincible(Handle, toggle);
        public void OnlyDamagedByPlayer(bool toggle) => SetEntityOnlyDamagedByPlayer(Handle, toggle);
        public int Opacity
        {
            get => GetEntityAlpha(Handle);
            set => SetEntityAlpha(Handle, value, false);
        }
        public void ResetOpacity() => ResetEntityAlpha(Handle);
        public bool HasCollided() => HasEntityCollidedWithAnything(Handle);
        public bool IsCollisionEnabled
        {
            get => GetEntityCollisonDisabled(Handle);
            set => SetEntityCollision(Handle, value, false);
        }
        public void IsRecordingCollisions(bool toggle) => SetEntityRecordsCollisions(Handle, toggle);
        public void SetNoCollision(EntityCore entity, bool toggle) => SetEntityNoCollisionEntity(Handle, entity.Handle, toggle);
        public bool HasBeenDamagedBy(EntityCore entity) => HasEntityBeenDamagedByEntity(Handle, entity.Handle, true);
        public bool HasBeenDamagedBy(WeaponHash weapon) => HasEntityBeenDamagedByWeapon(Handle, (uint)weapon, 0);
        public bool HasBeenDamagedByAnyWeapon() => HasEntityBeenDamagedByWeapon(Handle, 0, 2);
        public bool HasBeenDamagedByAnyMeleeWeapon() => HasEntityBeenDamagedByWeapon(Handle, 0, 1);
        public void ClearLastWeaponDamage() => ClearEntityLastWeaponDamage(Handle);
        public bool IsInArea(Vector3 minBounds, Vector3 maxBounds) => IsEntityInArea(Handle, minBounds.X, minBounds.Y, minBounds.Z, maxBounds.X, maxBounds.Y, maxBounds.Z, false, true, 0);
        public bool IsInAngledArea(Vector3 origin, Vector3 edge, float angle) => IsEntityInAngledArea(Handle, origin.X, origin.Y, origin.Z, edge.X, edge.Y, edge.Z, angle, false, true, 0);
        public bool IsInRangeOf(Vector3 position, float range) => Vector3.Subtract(Position, position).LengthSquared() < range * range;
        public bool IsNearEntity(EntityCore entity, Vector3 bounds) => IsEntityAtEntity(Handle, entity.Handle, bounds.X, bounds.Y, bounds.Z, false, true, 0);
    }
}
