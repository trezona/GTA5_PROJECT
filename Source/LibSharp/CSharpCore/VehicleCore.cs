﻿using CitizenFX.Core;
using System;
using System.Threading.Tasks;
using static LibSharp.CSharpCore.Core;
using CitizenFX.Core.Native;

namespace LibSharp.CSharpCore
{
    public enum RadioNames
    {
        RADIO_01_CLASS_ROCK = 0, // Los Santos Rock Radio
        RADIO_02_POP = 1, // Non-Stop-Pop FM
        RADIO_03_HIPHOP_NEW = 2, // Radio Los Santos
        RADIO_04_PUNK = 3, // Channel X
        RADIO_05_TALK_01 = 4, // West Coast Talk Radio
        RADIO_06_COUNTRY = 5, // Rebel Radio
        RADIO_07_DANCE_01 = 6, // Soulwax FM
        RADIO_08_MEXICAN = 7, // East Los FM
        RADIO_09_HIPHOP_OLD = 8, // West Coast Classics
        RADIO_12_REGGAE = 9, // Blue Ark
        RADIO_13_JAZZ = 10, // Worldwide FM
        RADIO_14_DANCE_02 = 11, // FlyLo FM
        RADIO_15_MOTOWN = 12, // The Lowdown 91.1
        RADIO_20_THELAB = 13,// The Lab
        RADIO_16_SILVERLAKE = 14, // Radio Mirror Park
        RADIO_17_FUNK = 15,// Space 103.2
        RADIO_18_90S_ROCK = 16, // Vinewood Boulevard Radio
        RADIO_19_USER = 17, // Self Radio
        RADIO_11_TALK_02 = 18, // Blaine County Radio
        HIDDEN_RADIO_AMBIENT_TV_BRIGHT = 19,
        HIDDEN_RADIO_AMBIENT_TV = 20,
        HIDDEN_RADIO_ADVERTS = 21,
        HIDDEN_RADIO_01_CLASS_ROCK = 22,
        HIDDEN_RADIO_02_POP = 23,
        HIDDEN_RADIO_03_HIPHOP_NEW = 24,
        HIDDEN_RADIO_04_PUNK = 25,
        HIDDEN_RADIO_06_COUNTRY = 26,
        HIDDEN_RADIO_07_DANCE_01 = 27,
        HIDDEN_RADIO_09_HIPHOP_OLD = 28,
        HIDDEN_RADIO_12_REGGAE = 29,
        HIDDEN_RADIO_15_MOTOWN = 30,
        HIDDEN_RADIO_16_SILVERLAKE = 31,
        HIDDEN_RADIO_STRIP_CLUB = 32,
        HIDDEN_RADIO_BIKER_CLASSIC_ROCK = 33,
        HIDDEN_RADIO_BIKER_MODERN_ROCK = 34,
        HIDDEN_RADIO_BIKER_PUNK = 35,
        HIDDEN_RADIO_BIKER_HIP_HOP = 36,
        OFF = 255
    }

    public class VehicleCore : EntityCore
    {
        _VehicleDoorCollection _doors;
        _VehicleModCollection _mods;
        _VehicleWheelCollection _wheels;
        _VehicleWindowCollection _windows;

        public VehicleCore(int handle) : base(handle) { }
        public string DisplayName() => GetModelDisplayName(Model);
        public string LocalizedName() => Game.GetGXTEntry(API.GetDisplayNameFromVehicleModel((uint)Model.Hash));
        public string ClassDisplayName() => GetClassDisplayName(ClassType());
        public string ClassLocalizedName() => Game.GetGXTEntry(ClassDisplayName());
        public VehicleClass ClassType() => (VehicleClass)API.GetVehicleClass(Handle);
        public float BodyHealth { get => API.GetVehicleBodyHealth(Handle); set => API.SetVehicleBodyHealth(Handle, value); }
        public float EngineHealth { get => API.GetVehicleEngineHealth(Handle); set => API.SetVehicleEngineHealth(Handle, value); }
        public float PetrolTankHealth { get => API.GetVehiclePetrolTankHealth(Handle); set => API.SetVehiclePetrolTankHealth(Handle, value); }
        public float FuelLevel { get => API.GetVehicleFuelLevel(Handle); set => API.SetVehicleFuelLevel(Handle, value); }
        public float OilLevel { get => API.GetVehicleOilLevel(Handle); set => API.SetVehicleOilLevel(Handle, value); }
        public float Gravity { get => API.GetVehicleGravityAmount(Handle); set => API.SetVehicleGravityAmount(Handle, value); }
        public bool IsEngineRunning { get => API.GetIsVehicleEngineRunning(Handle); set => API.SetVehicleEngineOn(Handle, value, true, true); }
        public bool IsEngineStarting() => API.IsVehicleEngineStarting(Handle);
        public void IsRadioEnabled(bool toggle) => API.SetVehicleRadioEnabled(Handle, toggle);
        public void RadioStation(RadioStation value)
        {
            if (value == CitizenFX.Core.RadioStation.RadioOff) API.SetVehRadioStation(Handle, "OFF");
            else if (Enum.IsDefined(typeof(RadioStation), value)) API.SetVehRadioStation(Handle, ((RadioNames)(int)value).ToString());
        }
        public float Speed
        {
            get => API.GetEntitySpeed(Handle);
            set
            {
                if (Model.IsTrain)
                {
                    API.SetTrainSpeed(Handle, value);
                    API.SetTrainCruiseSpeed(Handle, value);
                }
                else API.SetVehicleForwardSpeed(Handle, value);
            }
        }
        public float Acceleration() => API.GetVehicleAcceleration(Handle);
        public float CurrentRPM { get => API.GetVehicleCurrentRpm(Handle); set => API.SetVehicleCurrentRpm(Handle, value); }
        public float HighGear { get => API.GetVehicleHighGear(Handle); set => API.SetVehicleHighGear(Handle, value); }
        public int CurrentGear() => API.GetVehicleCurrentGear(Handle);
        public float SteeringAngle() => API.GetVehicleSteeringAngle(Handle);
        public float SteeringScale { get => API.GetVehicleSteeringScale(Handle); set => API.SetVehicleSteeringScale(Handle, value); }
        public bool HasForks() => Bones.HasBone("forks");
        public bool IsAlarm { get => API.IsVehicleAlarmActivated(Handle); set => API.SetVehicleAlarm(Handle, value); }
        public int AlarmTimeLeft { get => API.GetVehicleAlarmTimeLeft(Handle); set => API.SetVehicleAlarmTimeLeft(Handle, value); }
        public void StartAlarm() => API.StartVehicleAlarm(Handle);
        public bool HasSiren() => Bones.HasBone("siren1");
        public bool IsSirenActive { get => API.IsVehicleSirenOn(Handle); set => API.SetVehicleSiren(Handle, value); }
        public void IsSirenSilent(bool toggle) => API.DisableVehicleImpactExplosionActivation(Handle, toggle);
        public void SoundHorn(int duration) => API.StartVehicleHorn(Handle, duration, (uint)Game.GenerateHash("HELDDOWN"), false);
        public bool IsWanted { get => API.IsVehicleWanted(Handle); set => API.SetVehicleIsWanted(Handle, value); }
        public void ProvidesCover(bool toggle) => API.SetVehicleProvidesCover(Handle, toggle);
        public void DropsMoneyOnExplosion(bool toggle) => API.SetVehicleCreatesMoneyPickupsWhenExploded(Handle, toggle);
        public bool IsOwner { get => API.IsVehiclePreviouslyOwnedByPlayer(Handle); set => API.SetVehicleHasBeenOwnedByPlayer(Handle, value); }
        public bool NeedsToBeHotwired { get => API.IsVehicleNeedsToBeHotwired(Handle); set => API.SetVehicleNeedsToBeHotwired(Handle, value); }
        public bool AreLightsOn
        {
            get
            {
                bool lightState1 = false, lightState2 = false;
                API.GetVehicleLightsState(Handle, ref lightState1, ref lightState2);
                return lightState1;
            }
            set => API.SetVehicleLights(Handle, value ? 3 : 4);
        }
        public bool AreHighBeamsOn
        {
            get
            {
                bool lightState1 = false, lightState2 = false;
                API.GetVehicleLightsState(Handle, ref lightState1, ref lightState2);
                return lightState2;
            }
            set => API.SetVehicleFullbeam(Handle, value);
        }
        public bool IsInteriorLightOn { get => API.IsVehicleInteriorLightOn(Handle); set => API.SetVehicleInteriorlight(Handle, value); }
        public bool IsSearchLightOn { get => API.IsVehicleSearchlightOn(Handle); set => API.SetVehicleSearchlight(Handle, value, false); }
        public bool IsTaxiLightOn { get => API.IsTaxiLightOn(Handle); set => API.SetTaxiLights(Handle, value); }
        public void IsLeftIndicatorLightOn(bool toggle) => API.SetVehicleIndicatorLights(Handle, 1, toggle);
        public void IsRightIndicatorLightOn(bool toggle) => API.SetVehicleIndicatorLights(Handle, 0, toggle);
        public bool IsHandbrake { get => API.GetVehicleHandbrake(Handle); set => API.SetVehicleHandbrake(Handle, value); }
        public void AreBrakeLightsOn(bool toggle) => API.SetVehicleBrakeLights(Handle, toggle);
        public void LightsMultiplier(float multiplier) => API.SetVehicleLightMultiplier(Handle, multiplier);
        public void CanBeVisiblyDamaged(bool toggle) => API.SetVehicleCanBeVisiblyDamaged(Handle, toggle);
        public bool IsDamaged() => API.IsVehicleDamaged(Handle);
        public bool IsDriveable { get => API.IsVehicleDriveable(Handle, false); set => API.SetVehicleUndriveable(Handle, !value); }
        public bool HasRoof() => API.DoesVehicleHaveRoof(Handle);
        public bool IsLeftHeadLightBroken() => API.GetIsLeftVehicleHeadlightDamaged(Handle);
        public bool IsRightHeadLightBroken() => API.GetIsRightVehicleHeadlightDamaged(Handle);
        public bool IsRearBumperBrokenOff() => API.IsVehicleBumperBrokenOff(Handle, false);
        public bool IsFrontBumperBrokenOff() => API.IsVehicleBumperBrokenOff(Handle, true);
        public void IsAxlesStrong(bool toggle) => API.SetVehicleHasStrongAxles(Handle, toggle);
        public void CanEngineDegrade(bool toggle) => API.SetVehicleEngineCanDegrade(Handle, toggle);
        public void EnginePowerMultiplier(float multiplier) => API.SetVehicleEnginePowerMultiplier(Handle, multiplier);
        public void EngineTorqueMultiplier(float multiplier) => API.SetVehicleEngineTorqueMultiplier(Handle, multiplier);
        public VehicleLandingGearState LandingGearState { get => (VehicleLandingGearState)API.GetLandingGearState(Handle); set => API.SetVehicleLandingGear(Handle, (int)value); }
        public VehicleRoofState RoofState
        {
            get => (VehicleRoofState)API.GetConvertibleRoofState(Handle);
            set
            {
                switch (value)
                {
                    case VehicleRoofState.Closed:
                        API.RaiseConvertibleRoof(Handle, true);
                        API.RaiseConvertibleRoof(Handle, false);
                        break;
                    case VehicleRoofState.Closing:
                        API.RaiseConvertibleRoof(Handle, false);
                        break;
                    case VehicleRoofState.Opened:
                        API.RaiseConvertibleRoof(Handle, true);
                        API.RaiseConvertibleRoof(Handle, false);
                        break;
                    case VehicleRoofState.Opening:
                        API.RaiseConvertibleRoof(Handle, false);
                        break;
                }
            }
        }
        public VehicleLockStatus LockStatus { get => (VehicleLockStatus)API.GetVehicleDoorLockStatus(Handle); set => API.SetVehicleDoorsLocked(Handle, (int)value); }
        public float MaxBraking() => API.GetVehicleMaxBraking(Handle);
        public float MaxTraction() => API.GetVehicleMaxTraction(Handle);
        public bool IsOnAllWheels() => API.IsVehicleOnAllWheels(Handle);
        public bool IsStopped() => API.IsVehicleStopped(Handle);
        public bool IsStoppedAtTrafficLights() => API.IsVehicleStoppedAtTrafficLights(Handle);
        public bool IsStolen { get => API.IsVehicleStolen(Handle); set => API.SetVehicleIsStolen(Handle, value); }
        public bool IsConvertible() => API.IsVehicleAConvertible(Handle, false);
        public bool IsBurnout { get => API.IsVehicleInBurnout(Handle); set => API.SetVehicleBurnout(Handle, value); }
        public Ped Driver() => GetPedOnSeat(VehicleSeat.Driver);
        public Ped[] Occupants()
        {
            Ped driver = Driver();

            if (!Ped.Exists(driver))
            {
                return Passengers();
            }

            var result = new Ped[PassengerCount() + 1];
            result[0] = driver;

            for (int i = 0, j = 0, seats = PassengerCapacity(); i < seats && j < result.Length; i++)
            {
                if (!IsSeatFree((VehicleSeat)i))
                {
                    result[j++ + 1] = GetPedOnSeat((VehicleSeat)i);
                }
            }
            return result;
        }
        public Ped[] Passengers()
        {
            var result = new Ped[PassengerCount()];

            if (result.Length == 0)
            {
                return result;
            }

            for (int i = 0, j = 0, seats = PassengerCapacity(); i < seats && j < result.Length; i++)
            {
                if (!IsSeatFree((VehicleSeat)i))
                {
                    result[j++] = GetPedOnSeat((VehicleSeat)i);
                }
            }
            return result;
        }
        public int PassengerCapacity() => API.GetVehicleMaxNumberOfPassengers(Handle);
        public int PassengerCount() => API.GetVehicleNumberOfPassengers(Handle);
        public _VehicleDoorCollection Doors()
        {
            if (_doors == null) _doors = new _VehicleDoorCollection(this);
            return _doors;
        }
        public _VehicleModCollection Mods()
        {
            if (_mods == null)
            {
                _mods = new _VehicleModCollection(this);
            }
            return _mods;
        }
        public _VehicleWheelCollection Wheels()
        {
            if (_wheels == null)
            {
                _wheels = new _VehicleWheelCollection(this);
            }
            return _wheels;
        }
        public _VehicleWindowCollection Windows()
        {
            if (_windows == null)
            {
                _windows = new _VehicleWindowCollection(this);
            }
            return _windows;
        }
        public bool ExtraExists(int extra) => API.DoesExtraExist(Handle, extra);
        public bool IsExtraOn(int extra) => API.IsVehicleExtraTurnedOn(Handle, extra);
        public void ToggleExtra(int extra, bool toggle) => API.SetVehicleExtra(Handle, extra, toggle);
        public Ped GetPedOnSeat(VehicleSeat seat) => new Ped(API.GetPedInVehicleSeat(Handle, (int)seat));
        public bool IsSeatFree(VehicleSeat seat) => API.IsVehicleSeatFree(Handle, (int)seat);
        public float DirtLevel { get => API.GetVehicleDirtLevel(Handle); set => API.SetVehicleDirtLevel(Handle, value); }
        public bool PlaceOnGround() => API.SetVehicleOnGroundProperly(Handle);
        public void Repair() => API.SetVehicleFixed(Handle);
        public void Explode() => API.ExplodeVehicle(Handle, true, false);
        public bool CanTiresBurst { get => API.GetVehicleTyresCanBurst(Handle); set => API.SetVehicleTyresCanBurst(Handle, value); }
        public void CanWheelsBreak(bool enabled) => API.SetVehicleWheelsCanBreak(Handle, enabled);
        public bool HasBombBay() => Bones.HasBone("door_hatch_l") && Bones.HasBone("door_hatch_r");
        public void OpenBombBay() { if (HasBombBay()) API.OpenBombBayDoors(Handle); }
        public void CloseBombBay() { if (HasBombBay()) API.CloseBombBayDoors(Handle); }
        public void SetHeliYawPitchRollMult(float mult) { if (Model.IsHelicopter && mult >= 0 && mult <= 1) API.SetHelicopterRollPitchYawMult(Handle, mult); }
        public void DropCargobobHook(CargobobHook hook) { if (Model.IsCargobob) API.EnableCargobobHook(Handle, (int)hook); }
        public void RetractCargobobHook() { if (Model.IsCargobob) API.RetractCargobobHook(Handle); }
        public bool IsCargobobHookActive()
        {
            if (Model.IsCargobob) return API.IsCargobobHookActive(Handle) || API.IsCargobobMagnetActive(Handle);
            return false;
        }
        public bool IsCargobobHookActive(CargobobHook hook)
        {
            if (Model.IsCargobob)
            {
                switch (hook)
                {
                    case CargobobHook.Hook:
                        return API.IsCargobobHookActive(Handle);
                    case CargobobHook.Magnet:
                        return API.IsCargobobMagnetActive(Handle);
                }
            }
            return false;
        }
        public void CargoBobMagnetGrabVehicle() { if (IsCargobobHookActive(CargobobHook.Magnet)) API.CargobobMagnetGrabVehicle(Handle, true); }
        public void CargoBobMagnetReleaseVehicle() { if (IsCargobobHookActive(CargobobHook.Magnet)) API.CargobobMagnetGrabVehicle(Handle, false); }
        public bool HasTowArm() => Bones.HasBone("tow_arm");
        public void TowingCraneRaisedAmount(float value) => API.SetTowTruckCraneHeight(Handle, value);
        public VehicleCore TowedVehicle() => new VehicleCore(API.GetEntityAttachedToTowTruck(Handle));
        public void TowVehicle(VehicleCore vehicle, bool rear) => API.AttachVehicleToTowTruck(Handle, vehicle.Handle, rear, 0, 0, 0);
        public void DetachFromTowTruck() => API.DetachVehicleFromAnyTowTruck(Handle);
        public void DetachTowedVehicle()
        {
            VehicleCore vehicle = TowedVehicle();
            if (Exists(vehicle)) API.DetachVehicleFromTowTruck(Handle, vehicle.Handle);
        }
        public void Deform(Vector3 position, float damageAmount, float radius) => API.SetVehicleDamage(Handle, position.X, position.Y, position.Z, damageAmount, radius, false);
        public async Task<Ped> CreatePedOnSeat(VehicleSeat seat, Model model)
        {
            if (!IsSeatFree(seat)) throw new ArgumentException("The VehicleSeat selected was not free", "seat");
            if (!model.IsPed || !await model.Request(1000)) return null;
            return new Ped(API.CreatePedInsideVehicle(Handle, 26, (uint)model.Hash, (int)seat, true, true));
        }
        public Ped CreateRandomPedOnSeat(VehicleSeat seat)
        {
            if (!IsSeatFree(seat)) throw new ArgumentException("The VehicleSeat selected was not free", "seat");
            if (seat == VehicleSeat.Driver) return new Ped(API.CreateRandomPedAsDriver(Handle, true));
            else
            {
                int pedHandle = API.CreateRandomPed(0, 0, 0);
                API.SetPedIntoVehicle(pedHandle, Handle, (int)seat);
                return new Ped(pedHandle);
            }
        }
        public static string GetModelDisplayName(Model vehicleModel) => API.GetDisplayNameFromVehicleModel((uint)vehicleModel.Hash);
        public static VehicleClass GetModelClass(Model vehicleModel) => (VehicleClass)API.GetVehicleClassFromName((uint)vehicleModel.Hash);
        public static string GetClassDisplayName(VehicleClass vehicleClass) => "VEH_CLASS_" + ((int)vehicleClass).ToString();
        public new bool Exists() => API.GetEntityType(Handle) == 2;
        public static bool Exists(VehicleCore vehicle) => !ReferenceEquals(vehicle, null) && vehicle.Exists();
    }
}
