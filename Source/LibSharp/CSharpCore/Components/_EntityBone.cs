﻿using CitizenFX.Core.Native;
using LibSharp.CSharpCore;

namespace CitizenFX.Core
{
    public class _EntityBone
    {
        protected readonly EntityCore _owner;
        protected readonly int _index;
        public _EntityBone(EntityCore owner, int boneIndex)
        {
            _owner = owner;
            _index = boneIndex;
        }
        public _EntityBone(EntityCore owner, string boneName)
        {
            _owner = owner;
            _index = Function.Call<int>(Hash.GET_ENTITY_BONE_INDEX_BY_NAME, owner, boneName);
        }
        public _EntityBone() { }
        public int Index => _index; 
        public EntityCore Owner => _owner;
        public Vector3 Position => API.GetWorldPositionOfEntityBone(_owner.Handle, _index);
        public bool IsValid => EntityCore.Exists(_owner) && _index != -1; 
        public bool Equals(_EntityBone entityBone) => !ReferenceEquals(entityBone, null) && _owner == entityBone.Owner && Index == entityBone.Index;
        public override bool Equals(object obj) =>  !ReferenceEquals(obj, null) && obj.GetType() == GetType() && Equals((EntityBone)obj);
        public override int GetHashCode() => base.GetHashCode();
        public static bool Exists(EntityBone entityBone) =>  !ReferenceEquals(entityBone, null) && entityBone.IsValid;
    }
}