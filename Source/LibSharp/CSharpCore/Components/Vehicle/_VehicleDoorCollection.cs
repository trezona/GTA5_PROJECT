﻿using LibSharp.CSharpCore;
using System;
using System.Collections.Generic;

namespace CitizenFX.Core
{
    public class _VehicleDoorCollection
    {
        VehicleCore _owner;
        readonly Dictionary<VehicleDoorIndex, _VehicleDoor> _vehicleDoors = new Dictionary<VehicleDoorIndex, _VehicleDoor>();
        internal _VehicleDoorCollection(VehicleCore owner) => _owner = owner;
        public _VehicleDoor this[VehicleDoorIndex index]
        {
            get
            {
                _VehicleDoor vehicleDoor = null;
                if (!_vehicleDoors.TryGetValue(index, out vehicleDoor))
                {
                    vehicleDoor = new _VehicleDoor(_owner, index);
                    _vehicleDoors.Add(index, vehicleDoor);
                }
                return vehicleDoor;
            }
        }
        public bool HasDoor(VehicleDoorIndex door)
        {
            switch (door)
            {
                case VehicleDoorIndex.FrontLeftDoor:
                    return _owner.Bones.HasBone("door_dside_f");
                case VehicleDoorIndex.FrontRightDoor:
                    return _owner.Bones.HasBone("door_pside_f");
                case VehicleDoorIndex.BackLeftDoor:
                    return _owner.Bones.HasBone("door_dside_r");
                case VehicleDoorIndex.BackRightDoor:
                    return _owner.Bones.HasBone("door_pside_r");
                case VehicleDoorIndex.Hood:
                    return _owner.Bones.HasBone("bonnet");
                case VehicleDoorIndex.Trunk:
                    return _owner.Bones.HasBone("boot");
            }
            return false;
        }
        public _VehicleDoor[] GetAll()
        {
            var result = new List<_VehicleDoor>();
            foreach (VehicleDoorIndex doorindex in Enum.GetValues(typeof(VehicleDoorIndex)))
            {
                if (HasDoor(doorindex)) result.Add(this[doorindex]);
            }
            return result.ToArray();
        }
        public IEnumerator<_VehicleDoor> GetEnumerator() => (GetAll() as IEnumerable<_VehicleDoor>).GetEnumerator();
    }
}