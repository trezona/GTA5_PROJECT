﻿using CitizenFX.Core.Native;
using LibSharp.CSharpCore;

namespace CitizenFX.Core
{
    public class _VehicleToggleMod
    {
        VehicleCore _owner;
        internal _VehicleToggleMod(VehicleCore owner, VehicleToggleModType modType)
        {
            _owner = owner;
            ModType = modType;
        }
        public VehicleToggleModType ModType { get; private set; }
        public bool IsInstalled
        {
            get => API.IsToggleModOn(_owner.Handle, (int)ModType);
            set => API.ToggleVehicleMod(_owner.Handle, (int)ModType, value);
        }
        public string LocalizedModTypeName => API.GetModSlotName(_owner.Handle, (int)ModType);
        public VehicleCore Vehicle => _owner;
        public void Remove() => API.RemoveVehicleMod(_owner.Handle, (int)ModType);
    }
}