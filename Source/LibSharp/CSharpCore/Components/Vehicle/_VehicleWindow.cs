﻿using CitizenFX.Core.Native;
using LibSharp.CSharpCore;

namespace CitizenFX.Core
{
    public class _VehicleWindow
    {
        VehicleCore _owner;
        internal _VehicleWindow(VehicleCore owner, VehicleWindowIndex index)
        {
            _owner = owner;
            Index = index;
        }
        public VehicleWindowIndex Index { get; private set; }
        public bool IsIntact => API.IsVehicleWindowIntact(_owner.Handle, (int)Index);
        public VehicleCore Vehicle => _owner;
        public void Repair() => API.FixVehicleWindow(_owner.Handle, (int)Index);
        public void Smash() => API.SmashVehicleWindow(_owner.Handle, (int)Index);
        public void RollUp() => API.RollUpWindow(_owner.Handle, (int)Index);
        public void RollDown() => API.RollDownWindow(_owner.Handle, (int)Index);
        public void Remove() => API.RemoveVehicleWindow(_owner.Handle, (int)Index);
    }
}