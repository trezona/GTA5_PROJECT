﻿using CitizenFX.Core.Native;
using LibSharp.CSharpCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace CitizenFX.Core
{
    public class _VehicleModCollection
    {
        VehicleCore _owner;
        readonly Dictionary<VehicleModType, _VehicleMod> _vehicleMods = new Dictionary<VehicleModType, _VehicleMod>();
        readonly Dictionary<VehicleToggleModType, _VehicleToggleMod> _vehicleToggleMods = new Dictionary<VehicleToggleModType, _VehicleToggleMod>();

        private static readonly ReadOnlyDictionary<VehicleWheelType, Tuple<string, string>> _wheelNames = new ReadOnlyDictionary
            <VehicleWheelType, Tuple<string, string>>(
            new Dictionary<VehicleWheelType, Tuple<string, string>>
            {
                {VehicleWheelType.BikeWheels, new Tuple<string, string>("CMOD_WHE1_0", "Bike")},
                {VehicleWheelType.HighEnd, new Tuple<string, string>("CMOD_WHE1_1", "High End")},
                {VehicleWheelType.Lowrider, new Tuple<string, string>("CMOD_WHE1_2", "Lowrider")},
                {VehicleWheelType.Muscle, new Tuple<string, string>("CMOD_WHE1_3", "Muscle")},
                {VehicleWheelType.Offroad, new Tuple<string, string>("CMOD_WHE1_4", "Offroad")},
                {VehicleWheelType.Sport, new Tuple<string, string>("CMOD_WHE1_5", "Sport")},
                {VehicleWheelType.SUV, new Tuple<string, string>("CMOD_WHE1_6", "SUV")},
                {VehicleWheelType.Tuner, new Tuple<string, string>("CMOD_WHE1_7", "Tuner")},
                {VehicleWheelType.BennysOriginals, new Tuple<string, string>("CMOD_WHE1_8", "Benny's Originals")},
                {VehicleWheelType.BennysBespoke, new Tuple<string, string>("CMOD_WHE1_9", "Benny's Bespoke")}
            });

        internal _VehicleModCollection(VehicleCore owner) => _owner = owner;
        public _VehicleMod this[VehicleModType modType]
        {
            get
            {
                _VehicleMod vehicleMod = null;
                if (!_vehicleMods.TryGetValue(modType, out vehicleMod))
                {
                    vehicleMod = new _VehicleMod(_owner, modType);
                    _vehicleMods.Add(modType, vehicleMod);
                }
                return vehicleMod;
            }
        }
        public _VehicleToggleMod this[VehicleToggleModType modType]
        {
            get
            {
                _VehicleToggleMod vehicleToggleMod = null;
                if (!_vehicleToggleMods.TryGetValue(modType, out vehicleToggleMod))
                {
                    vehicleToggleMod = new _VehicleToggleMod(_owner, modType);
                    _vehicleToggleMods.Add(modType, vehicleToggleMod);
                }
                return vehicleToggleMod;
            }
        }
        public bool HasVehicleMod(VehicleModType type) => API.GetNumVehicleMods(_owner.Handle, (int)type) > 0;
        public _VehicleMod[] GetAllMods() => Enum.GetValues(typeof(VehicleModType)).Cast<VehicleModType>().Where(HasVehicleMod).Select(modType => this[modType]).ToArray();
        public VehicleWheelType WheelType
        {
            get => (VehicleWheelType)API.GetVehicleWheelType(_owner.Handle);
            set => API.SetVehicleWheelType(_owner.Handle, (int)value);
        }
        public VehicleWheelType[] AllowedWheelTypes
        {
            get
            {
                if (_owner.Model.IsBicycle || _owner.Model.IsBike) return new VehicleWheelType[] { VehicleWheelType.BikeWheels };
                if (_owner.Model.IsCar)
                {
                    var res = new List<VehicleWheelType>()
                    {
                        VehicleWheelType.Sport,
                        VehicleWheelType.Muscle,
                        VehicleWheelType.Lowrider,
                        VehicleWheelType.SUV,
                        VehicleWheelType.Offroad,
                        VehicleWheelType.Tuner,
                        VehicleWheelType.HighEnd
                    };
                    switch ((VehicleHash)_owner.Model)
                    {
                        case VehicleHash.Faction2:
                        case VehicleHash.Buccaneer2:
                        case VehicleHash.Chino2:
                        case VehicleHash.Moonbeam2:
                        case VehicleHash.Primo2:
                        case VehicleHash.Voodoo2:
                        case VehicleHash.SabreGT2:
                        case VehicleHash.Tornado5:
                        case VehicleHash.Virgo2:
                        case VehicleHash.Minivan2:
                        case VehicleHash.SlamVan3:
                        case VehicleHash.Faction3:
                            res.AddRange(new VehicleWheelType[] { VehicleWheelType.BennysOriginals, VehicleWheelType.BennysBespoke });
                            break;
                        case VehicleHash.SultanRS:
                        case VehicleHash.Banshee2:
                            res.Add(VehicleWheelType.BennysOriginals);
                            break;
                    }
                    return res.ToArray();
                }
                return new VehicleWheelType[0];
            }
        }
        public string LocalizedWheelTypeName => GetLocalizedWheelTypeName(WheelType);
        public string GetLocalizedWheelTypeName(VehicleWheelType wheelType)
        {
            if (!API.HasThisAdditionalTextLoaded("mod_mnu", 10))
            {
                API.ClearAdditionalText(10, true);
                API.RequestAdditionalText("mod_mnu", 10);
            }
            if (_wheelNames.ContainsKey(wheelType))
            {
                if (Game.DoesGXTEntryExist(_wheelNames[wheelType].Item1)) return Game.GetGXTEntry(_wheelNames[wheelType].Item1);
                return _wheelNames[wheelType].Item2;
            }
            throw new ArgumentException("Wheel Type is undefined", "wheelType");
        }
        public void InstallModKit() => API.SetVehicleModKit(_owner.Handle, 0);
        public async Task<bool> RequestAdditionTextFile(int timeout = 1000)
        {
            if (!API.HasThisAdditionalTextLoaded("mod_mnu", 10))
            {
                API.ClearAdditionalText(10, true);
                API.RequestAdditionalText("mod_mnu", 10);
                int end = Game.GameTime + timeout;
                {
                    while (Game.GameTime < end)
                    {
                        if (API.HasThisAdditionalTextLoaded("mod_mnu", 10)) return true;
                        await BaseScript.Delay(0);
                    }
                    return false;
                }
            }
            return true;
        }
        public int Livery
        {
            get
            {
                int modCount = this[VehicleModType.Livery].ModCount;
                if (modCount > 0) return modCount;
                return API.GetVehicleLivery(_owner.Handle);
            }
            set
            {
                if (this[VehicleModType.Livery].ModCount > 0) this[VehicleModType.Livery].Index = value;
                else API.SetVehicleLivery(_owner.Handle, value);
            }
        }
        public int LiveryCount
        {
            get
            {
                int modCount = this[VehicleModType.Livery].ModCount;
                if (modCount > 0) return modCount;
                return API.GetVehicleLiveryCount(_owner.Handle);
            }
        }
        public string LocalizedLiveryName => GetLocalizedLiveryName(Livery);
        public string GetLocalizedLiveryName(int index)
        {
            int modCount = this[VehicleModType.Livery].ModCount;
            if (modCount > 0) return this[VehicleModType.Livery].GetLocalizedModName(index);
            return Game.GetGXTEntry(API.GetLiveryName(_owner.Handle, index));
        }
        public VehicleWindowTint WindowTint
        {
            get => (VehicleWindowTint)API.GetVehicleWindowTint(_owner.Handle);
            set => API.SetVehicleWindowTint(_owner.Handle, (int)value);
        }
        public VehicleColor PrimaryColor
        {
            get
            {
                int color1 = 0, color2 = 0;
                API.GetVehicleColours(_owner.Handle, ref color1, ref color2);
                return (VehicleColor)color1;
            }
            set => API.SetVehicleColours(_owner.Handle, (int)value, (int)SecondaryColor);
        }
        public VehicleColor SecondaryColor
        {
            get
            {
                int color1 = 0, color2 = 0;
                API.GetVehicleColours(_owner.Handle, ref color1, ref color2);
                return (VehicleColor)color2;
            }
            set => API.SetVehicleColours(_owner.Handle, (int)PrimaryColor, (int)value);
        }
        public VehicleColor RimColor
        {
            get
            {
                int color1 = 0, color2 = 0;
                API.GetVehicleExtraColours(_owner.Handle, ref color1, ref color2);
                return (VehicleColor)color2;
            }
            set => API.SetVehicleExtraColours(_owner.Handle, (int)PearlescentColor, (int)value);
        }
        public VehicleColor PearlescentColor
        {
            get
            {
                int color1 = 0, color2 = 0;
                API.GetVehicleExtraColours(_owner.Handle, ref color1, ref color2);
                return (VehicleColor)color1;
            }
            set => API.SetVehicleExtraColours(_owner.Handle, (int)value, (int)RimColor);
        }
        public VehicleColor TrimColor
        {
            get
            {
                int color = 0, paintType = 0, p3 = 0;
                API.GetVehicleModColor_1(_owner.Handle, ref paintType, ref color, ref p3);
                return (VehicleColor)color;
            }
            set => API.SetVehicleModColor_1(_owner.Handle, 0, (int)value, 0);// Function.Call((Hash)17585947422526242585uL, _owner.Handle, value);
        }
        public VehicleColor DashboardColor
        {
            get
            {
                int color = 0, paintType = 0;
                API.GetVehicleModColor_2(_owner.Handle, ref paintType, ref color);
                return (VehicleColor)color;
            }
            set => API.SetVehicleModColor_2(_owner.Handle, 0, (int)value);// Function.Call((Hash)6956317558672667244uL, _owner.Handle, value);
        }
        public int ColorCombination
        {
            get => API.GetVehicleColourCombination(_owner.Handle);
            set => API.SetVehicleColourCombination(_owner.Handle, value);
        }
        public int ColorCombinationCount => API.GetNumberOfVehicleColours(_owner.Handle);
        public Color TireSmokeColor
        {
            get
            {
                int red = 0, green = 0, blue = 0;
                API.GetVehicleTyreSmokeColor(_owner.Handle, ref red, ref green, ref blue);
                return Color.FromArgb(red, green, blue);
            }
            set => API.SetVehicleTyreSmokeColor(_owner.Handle, value.R, value.G, value.B);
        }
        public Color NeonLightsColor
        {
            get
            {
                int red = 0, green = 0, blue = 0;
                API.GetVehicleNeonLightsColour(_owner.Handle, ref red, ref green, ref blue);
                return Color.FromArgb(red, green, blue);
            }
            set => API.SetVehicleNeonLightsColour(_owner.Handle, value.R, value.G, value.B);
        }
        public bool IsNeonLightsOn(VehicleNeonLight light) => API.IsVehicleNeonLightEnabled(_owner.Handle, (int)light);
        public void SetNeonLightsOn(VehicleNeonLight light, bool on) => API.SetVehicleNeonLightEnabled(_owner.Handle, (int)light, on);
        public bool HasNeonLights => Enum.GetValues(typeof(VehicleNeonLight)).Cast<VehicleNeonLight>().Any(HasNeonLight);
        public bool HasNeonLight(VehicleNeonLight neonLight)
        {
            switch (neonLight)
            {
                case VehicleNeonLight.Left:
                    return _owner.Bones.HasBone("neon_l");
                case VehicleNeonLight.Right:
                    return _owner.Bones.HasBone("neon_r");
                case VehicleNeonLight.Front:
                    return _owner.Bones.HasBone("neon_f");
                case VehicleNeonLight.Back:
                    return _owner.Bones.HasBone("neon_b");
                default:
                    return false;
            }
        }
        public Color CustomPrimaryColor
        {
            get
            {
                int red = 0, green = 0, blue = 0;
                API.GetVehicleCustomPrimaryColour(_owner.Handle, ref red, ref green, ref blue);
                return Color.FromArgb(red, green, blue);
            }
            set => API.SetVehicleCustomPrimaryColour(_owner.Handle, value.R, value.G, value.B);
        }
        public Color CustomSecondaryColor
        {
            get
            {
                int red = 0, green = 0, blue = 0;
                API.GetVehicleCustomSecondaryColour(_owner.Handle, ref red, ref green, ref blue);
                return Color.FromArgb(red, green, blue);
            }
            set => API.SetVehicleCustomSecondaryColour(_owner.Handle, value.R, value.G, value.B);
        }
        public bool IsPrimaryColorCustom => API.GetIsVehiclePrimaryColourCustom(_owner.Handle);
        public bool IsSecondaryColorCustom => API.GetIsVehicleSecondaryColourCustom(_owner.Handle);
        public void ClearCustomPrimaryColor() => API.ClearVehicleCustomPrimaryColour(_owner.Handle);
        public void ClearCustomSecondaryColor() => API.ClearVehicleCustomSecondaryColour(_owner.Handle);
        public LicensePlateStyle LicensePlateStyle
        {
            get => (LicensePlateStyle)API.GetVehicleNumberPlateTextIndex(_owner.Handle);
            set => API.SetVehicleNumberPlateTextIndex(_owner.Handle, (int)value);
        }
        public LicensePlateType LicensePlateType => (LicensePlateType)API.GetVehiclePlateType(_owner.Handle);
        public string LicensePlate
        {
            get => API.GetVehicleNumberPlateText(_owner.Handle);
            set => API.SetVehicleNumberPlateText(_owner.Handle, value);
        }
    }
}