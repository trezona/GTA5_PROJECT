﻿using CitizenFX.Core.Native;
using LibSharp.CSharpCore;

namespace CitizenFX.Core
{
    public class _VehicleWheel
    {
        VehicleCore _owner;
        internal _VehicleWheel(VehicleCore owner, int index)
        {
            _owner = owner;
            Index = index;
        }
        public int Index { get; private set; }
        public VehicleCore Vehicle =>  _owner;
        public void Burst() => API.SetVehicleTyreBurst(_owner.Handle, Index, true, 1000f);
        public void Fix() => API.SetVehicleTyreFixed(_owner.Handle, Index);
    }
}