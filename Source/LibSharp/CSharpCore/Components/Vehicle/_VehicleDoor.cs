﻿using System;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;

namespace CitizenFX.Core
{
    public class _VehicleDoor
    {
        VehicleCore _owner;
        internal _VehicleDoor(VehicleCore owner, VehicleDoorIndex index)
        {
            _owner = owner;
            Index = index;
        }
        public VehicleDoorIndex Index { get; private set; }
        public float AngleRatio
        {
            get => API.GetVehicleDoorAngleRatio(_owner.Handle, (int)Index);
            set => API.SetVehicleDoorControl(_owner.Handle, (int)Index, 1, value);
        }
        public void CanBeBroken(bool toggle) => API.SetVehicleDoorBreakable(_owner.Handle, (int)Index, toggle);
        public bool IsOpen => AngleRatio > 0;
        public bool IsFullyOpen => API.IsVehicleDoorFullyOpen(_owner.Handle, (int)Index);
        public bool IsBroken => API.IsVehicleDoorDamaged(_owner.Handle, (int)Index);
        public VehicleCore Vehicle => _owner;
        public void Open(bool loose = false, bool instantly = false) => API.SetVehicleDoorOpen(_owner.Handle, (int)Index, loose, instantly);
        public void Close(bool instantly = false) => API.SetVehicleDoorShut(_owner.Handle, (int)Index, instantly);
        public void Break(bool isBreak = true) => API.SetVehicleDoorBroken(_owner.Handle, (int)Index, isBreak);
    }
}