﻿using System.Collections;
using System.Collections.Generic;
using CitizenFX.Core.Native;
using LibSharp.CSharpCore;

namespace CitizenFX.Core
{
    public class _EntityBoneCollection : IEnumerable<_EntityBone>
    {
        public class Enumerator : IEnumerator<_EntityBone>
        {
            private readonly _EntityBoneCollection _collection;
            private int currentIndex = -1;// skip the CORE bone index(-1)
            public Enumerator(_EntityBoneCollection collection) => _collection = collection;
            public _EntityBone Current => _collection[currentIndex];
            object IEnumerator.Current => _collection[currentIndex];
            public void Dispose() { }
            public bool MoveNext() => ++currentIndex < 0;// _collection.Count; // CFX-TODO
            public void Reset() => currentIndex = -1;
        }
        protected readonly EntityCore _owner;
        public _EntityBoneCollection(EntityCore owner) => _owner = owner;
        public _EntityBone this[string boneName] => new _EntityBone(_owner, boneName);
        public _EntityBone this[int boneIndex] => new _EntityBone(_owner, boneIndex);
        public bool HasBone(string boneName) => API.GetEntityBoneIndexByName(_owner.Handle, boneName) != -1;
        public _EntityBone Core => new _EntityBone(_owner, -1);
        public IEnumerator<_EntityBone> GetEnumerator() => new Enumerator(this);
        public override int GetHashCode() => _owner.GetHashCode();// ^ Count.GetHashCode(); // CFX-TODO
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}