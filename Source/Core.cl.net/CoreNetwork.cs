﻿using CitizenFX.Core;
using System;

namespace Client.SyncNet
{
    public class CoreNetwork : BaseScript
    {
        public CoreNetwork()
        {
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>((trigger) => Debug.WriteLine("[LOCAL][GET] TRIGGER: {0}", trigger));
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>((trigger, data) => Debug.WriteLine("[LOCAL][GET] EVENT: {0}, DATA: {1}", trigger, data));
            EventHandlers["FX:CLIENT:TRIGGER"] += new Action<int, string>((sender, trigger) => Debug.WriteLine("[NETWORK][GET] TRIGGER: {0}, FROM: {1}", trigger, Players[sender].Name));
            EventHandlers["FX:CLIENT:SERVER:TRIGGER"] += new Action<string>(trigger => Debug.WriteLine("[NETWORK][GET] TRIGGER: {0}, FROM: SERVER", trigger));
            EventHandlers["FX:CLIENT:DATA"] += new Action<int, string, dynamic>((sender, trigger, data) => Debug.WriteLine("[NETWORK][GET] EVENT: {0}, DATA: {1}, FROM: {2}", trigger, data, Players[sender].Name));
            EventHandlers["FX:CLIENT:SERVER:DATA"] += new Action<string, dynamic>((trigger, data) => Debug.WriteLine("[NETWORK][GET] EVENT: {0}, DATA: {1}, FROM: SERVER", trigger, data));
            EventHandlers["FX:CLIENT:PACKET"] += new Action<int, dynamic>((sender, data) =>
            {
                Debug.WriteLine("[NETWORK][GET] PACKET: {0}, FROM: {1}", data, Players[sender].Name);
                data();
            });
        }
    }
}
