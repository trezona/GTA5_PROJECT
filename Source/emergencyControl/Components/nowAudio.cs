﻿using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace ClientGame.Components
{

    public static class nowAudio
    {
        #region Fields
        internal static readonly string[] _audioFlags = {
            "ActivateSwitchWheelAudio",
            "AllowCutsceneOverScreenFade",
            "AllowForceRadioAfterRetune",
            "AllowPainAndAmbientSpeechToPlayDuringCutscene",
            "AllowPlayerAIOnMission",
            "AllowPoliceScannerWhenPlayerHasNoControl",
            "AllowRadioDuringSwitch",
            "AllowRadioOverScreenFade",
            "AllowScoreAndRadio",
            "AllowScriptedSpeechInSlowMo",
            "AvoidMissionCompleteDelay",
            "DisableAbortConversationForDeathAndInjury",
            "DisableAbortConversationForRagdoll",
            "DisableBarks",
            "DisableFlightMusic",
            "DisableReplayScriptStreamRecording",
            "EnableHeadsetBeep",
            "ForceConversationInterrupt",
            "ForceSeamlessRadioSwitch",
            "ForceSniperAudio",
            "FrontendRadioDisabled",
            "HoldMissionCompleteWhenPrepared",
            "IsDirectorModeActive",
            "IsPlayerOnMissionForSpeech",
            "ListenerReverbDisabled",
            "LoadMPData",
            "MobileRadioInGame",
            "OnlyAllowScriptTriggerPoliceScanner",
            "PlayMenuMusic",
            "PoliceScannerDisabled",
            "ScriptedConvListenerMaySpeak",
            "SpeechDucksScore",
            "SuppressPlayerScubaBreathing",
            "WantedMusicDisabled",
            "WantedMusicOnMission"
        };
        #endregion

        public static int PlaySoundAt(Vector3 position, string sound, string set)
        {
            var soundId = Function.Call<int>(Hash.GET_SOUND_ID);
            Function.Call(Hash.PLAY_SOUND_FROM_COORD, soundId, sound, position.X, position.Y, position.Z, set, 0, 0, 0);
            return soundId;
        }
        public static int PlaySoundAt(Vector3 position, string sound)
        {
            var soundId = Function.Call<int>(Hash.GET_SOUND_ID);
            Function.Call(Hash.PLAY_SOUND_FROM_COORD, soundId, sound, position.X, position.Y, position.Z, 0, 0, 0, 0);
            return soundId;
        }
        public static int PlaySoundFromEntity(Entity entity, string sound, string set)
        {
            var soundId = Function.Call<int>(Hash.GET_SOUND_ID);
            Function.Call(Hash.PLAY_SOUND_FROM_ENTITY, soundId, sound, entity.Handle, set, 0, 0);
            return soundId;
        }
        public static int PlaySoundFromEntity(Entity entity, string sound)
        {
            var soundId = Function.Call<int>(Hash.GET_SOUND_ID);
            Function.Call(Hash.PLAY_SOUND_FROM_ENTITY, soundId, sound, entity.Handle, 0, 0, 0);
            return soundId;
        }
        public static int PlaySoundFrontend(string sound, string set)
        {
            var soundId = Function.Call<int>(Hash.GET_SOUND_ID);
            Function.Call(Hash.PLAY_SOUND_FRONTEND, soundId, sound, set, 0);
            return soundId;
        }
        public static int PlaySoundFrontend(string sound)
        {
            var soundId = Function.Call<int>(Hash.GET_SOUND_ID);
            Function.Call(Hash.PLAY_SOUND_FRONTEND, soundId, sound, 0, 0);
            return soundId;
        }

        public static void StopSound(int id)
        {
            Function.Call(Hash.STOP_SOUND, id);
        }
        public static void ReleaseSound(int id)
        {
            Function.Call(Hash.RELEASE_SOUND_ID, id);
        }

        public static bool HasSoundFinished(int id)
        {
            return Function.Call<bool>(Hash.HAS_SOUND_FINISHED, id);
        }

        public static void SetAudioFlag(string flag, bool toggle)
        {
            Function.Call(Hash.SET_AUDIO_FLAG, flag, toggle);
        }
        public static void SetAudioFlag(AudioFlag flag, bool toggle)
        {
            SetAudioFlag(_audioFlags[(int)flag], toggle);
        }
    }
}