﻿using CitizenFX.Core;
using System;

namespace ClientGame.Components
{
    public class Network : BaseScript
    {
        public Network()
        {
            //Trigger
            EventHandlers["FX:CLIENT:TRIGGER"] += new Action<int, string>((sender, trigger) => onNetworkTrigger(Players[sender], trigger));
            EventHandlers["FX:CLIENT:SERVER:TRIGGER"] += new Action<string>(trigger => onServerTriggerPrivate(trigger));

            //Data
            EventHandlers["FX:CLIENT:DATA"] += new Action<int, string, dynamic>((sender, trigger, data) => onNetworkData(Players[sender], trigger, data));
            EventHandlers["FX:CLIENT:SERVER:DATA"] += new Action<string, dynamic>((trigger, data) => onServerData(trigger, data));

            //Local
            EventHandlers["FX:LOCAL:TRIGGER"] += new Action<string>(trigger => onLocalTrigger(trigger));
            EventHandlers["FX:LOCAL:DATA"] += new Action<string, dynamic>((trigger, data) => onLocalData(trigger, data));
        }

        //LOCAL TRIGGER
        public delegate void onLocalTriggerDelegate(string triggerName);
        static private void onLocalTriggerHandler(string triggerName) { }
        static public event onLocalTriggerDelegate onLocalTrigger = new onLocalTriggerDelegate(onLocalTriggerHandler);

        //LOCAL DATA
        public delegate void onLocalDataDelegate(string triggerName, dynamic data);
        static private void onLocalDataHandler(string triggerName, dynamic data) { }
        static public event onLocalDataDelegate onLocalData = new onLocalDataDelegate(onLocalDataHandler);

        //TRIGGER
        public delegate void onNetworkTriggerDelegate(Player sender, string triggerName);
        static private void onNetworkTriggerHandler(Player sender, string triggerName) { }
        static public event onNetworkTriggerDelegate onNetworkTrigger = new onNetworkTriggerDelegate(onNetworkTriggerHandler);

        //TRIGGER SERVER
        public delegate void onServerTriggerPrivateDelegate(string triggerName);
        static private void onServerTriggerPrivateHandler(string triggerName) { }
        static public event onServerTriggerPrivateDelegate onServerTriggerPrivate = new onServerTriggerPrivateDelegate(onServerTriggerPrivateHandler);

        //DATA
        public delegate void onNetworkDataDelegate(Player sender, string triggerName, dynamic data);
        static private void onNetworkDataHandler(Player sender, string triggerName, dynamic data) { }
        static public event onNetworkDataDelegate onNetworkData = new onNetworkDataDelegate(onNetworkDataHandler);

        //DATA SERVER
        public delegate void onServerDataDelegate(string triggerName, dynamic data);
        static private void onServerDataHandler(string triggerName, dynamic data) { }
        static public event onServerDataDelegate onServerData = new onServerDataDelegate(onServerDataHandler);

        //== [ CMD ] ==

        //LOCAL CMD
        static public void Local(string triggerName)
        {
            TriggerEvent("FX:LOCAL:TRIGGER", triggerName);
            Debug.WriteLine("[LOCAL][SEND] TRIGGER: {0}", triggerName);
        }
        static public void Local<T>(string triggerName, T data)
        {
            TriggerEvent("FX:LOCAL:DATA", triggerName, data);
            Debug.WriteLine("[LOCAL][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }
        static public void Local<T>(string triggerName, params T[] data) => Local(triggerName, data);

        //SERVER CMD
        static public void Server(string triggerName)
        {
            TriggerServerEvent("FX:SERVER:CLIENT:TRIGGER", triggerName);
            Debug.WriteLine("[SERVER][SEND] TRIGGER: {0}", triggerName);
        }
        static public void Server<T>(string triggerName, T data)
        {
            TriggerServerEvent("FX:SERVER:CLIENT:DATA", triggerName, data);
            Debug.WriteLine("[SERVER][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}", triggerName, data.GetType().Name, data);
        }
        static public void Server<T>(string triggerName, params T[] data) => Local(triggerName, data);

        //TRIGGER
        static public void Send(Player player, string triggerName)
        {
            TriggerServerEvent("FX:SERVER:TRIGGER", triggerName, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] TRIGGER: {0}, FOR: {1}", triggerName, player.Name);
        }
        static public void Send(string triggerName) => Send(Game.Player, triggerName);

        //PACKET
        static public void Send(Player player, Action data)
        {
            TriggerServerEvent("FX:SERVER:PACKET", data, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] PACKET: {0}, FOR: {1}", data, player.Name);
        }
        static public void Send(Action data) => Send(Game.Player, data);

        //DATA
        static public void Send<T>(Player player, string triggerName, T data)
        {
            TriggerServerEvent("FX:SERVER:DATA", triggerName, data, player.ServerId);
            Debug.WriteLine("[NETWORK][SEND] EVENT: {0}, TYPE: {1}, DATA: {2}, FOR: {3}", triggerName, data.GetType().Name, data, player.Name);
        }
        static public void Send<T>(string triggerName, T data) => Send(Game.Player, triggerName, data);
        static public void Send<T>(string triggerName, params T[] data) => Send(triggerName, data);
        static public void Send<T>(Player player, string triggerName, params T[] data) => Send(player, triggerName, data);
    }
}
