﻿using CitizenFX.Core;
using ClientGame.Components;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using CitizenFX.Core.Native;

public class Init : BaseScript
{
    int count_bcast_timer = 0;
    int delay_bcast_timer = 200;

    int count_sndclean_timer = 0;
    int delay_sndclean_timer = 400;

    bool actv_ind_timer = false;
    int count_ind_timer = 0;
    int delay_ind_timer = 180;

    bool actv_lxsrnmute_temp = false;
    int srntone_temp = 0;
    bool dsrn_mute = true;

    // these models will use their real wail siren, as determined by their assigned audio hash in vehicles.meta
    string[] eModelsWithFireSrn = new string[]
    {
        "FIRETRUK",
    };
    // models listed below will use AMBULANCE_WARNING as auxiliary siren
    // unlisted models will instead use the default wail as the auxiliary siren
    string[] eModelsWithPcall = new string[]
    {
        "AMBULANCE",
        "FIRETRUK",
        "LGUARD",
    };

    string[] eSoundList = new string[]
    {
        "SIRENS_AIRHORN",
        "VEHICLES_HORNS_SIREN_1",
        "VEHICLES_HORNS_SIREN_2",
        "VEHICLES_HORNS_POLICE_WARNING",
        "VEHICLES_HORNS_AMBULANCE_WARNING"
    };

    Dictionary<Vehicle, int> state_indic = new Dictionary<Vehicle, int>();
    Dictionary<Vehicle, int> state_lxsiren = new Dictionary<Vehicle, int>();
    Dictionary<Vehicle, bool> state_pwrcall = new Dictionary<Vehicle, bool>();
    Dictionary<Vehicle, int> state_airmanu = new Dictionary<Vehicle, int>();

    const int ind_state_o = 0;
    const int ind_state_l = 1;
    const int ind_state_r = 2;
    const int ind_state_h = 3;

    Dictionary<Vehicle, int> snd_lxsiren = new Dictionary<Vehicle, int>();
    Dictionary<Vehicle, int> snd_pwrcall = new Dictionary<Vehicle, int>();
    Dictionary<Vehicle, int> snd_airmanu = new Dictionary<Vehicle, int>();

    bool useFiretruckSiren(Vehicle veh)
    {
        try
        {
            var model = ((VehicleHash)veh.Model.Hash).ToString();
            foreach (var raw in eModelsWithFireSrn)
            {
                if (raw.Equals(model, StringComparison.CurrentCultureIgnoreCase)) return true;
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
        return false;
    }

    bool usePowercallAuxSrn(Vehicle veh)
    {
        try
        {
            var model = ((VehicleHash)veh.Model.Hash).ToString();
            foreach (var raw in eModelsWithPcall)
            {
                if (raw.Equals(model, StringComparison.CurrentCultureIgnoreCase)) return true;
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
        return false;
    }

    void CleanupSounds()
    {
        try
        {
            if (count_sndclean_timer > delay_sndclean_timer)
            {
                count_sndclean_timer = 0;
                foreach (var raw in state_lxsiren)
                {
                    if (raw.Value > 0)
                    {
                        if (!raw.Key.Exists() || raw.Key.IsDead)
                        {
                            if (snd_lxsiren.ContainsKey(raw.Key))
                            {
                                nowAudio.StopSound(snd_lxsiren[raw.Key]);
                                nowAudio.ReleaseSound(snd_lxsiren[raw.Key]);
                                snd_lxsiren.Remove(raw.Key);
                                state_lxsiren.Remove(raw.Key);
                            }
                        }
                    }
                }
                foreach (var raw in state_pwrcall)
                {
                    if (raw.Value)
                    {
                        if (!raw.Key.Exists() || raw.Key.IsDead)
                        {
                            if (snd_pwrcall.ContainsKey(raw.Key))
                            {
                                nowAudio.StopSound(snd_pwrcall[raw.Key]);
                                nowAudio.ReleaseSound(snd_pwrcall[raw.Key]);
                                snd_pwrcall.Remove(raw.Key);
                                state_pwrcall.Remove(raw.Key);
                            }
                        }
                    }
                }
                foreach (var raw in state_airmanu)
                {
                    if (raw.Value > 0)
                    {
                        if (!raw.Key.Exists() || raw.Key.IsDead || raw.Key.IsSeatFree(VehicleSeat.Driver))
                        {
                            if (snd_airmanu.ContainsKey(raw.Key))
                            {
                                nowAudio.StopSound(snd_airmanu[raw.Key]);
                                nowAudio.ReleaseSound(snd_airmanu[raw.Key]);
                                snd_airmanu.Remove(raw.Key);
                                state_airmanu.Remove(raw.Key);
                            }
                        }
                    }
                }
            }
            else count_sndclean_timer = count_sndclean_timer + 1;
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    //Поворотники
    void TogIndicStateForVeh(Vehicle veh, int newstate)
    {
        try
        {
            if (veh.Exists() || veh.IsAlive)
            {
                switch (newstate)
                {
                    case ind_state_o:
                        veh.IsRightIndicatorLightOn = false;
                        veh.IsLeftIndicatorLightOn = false;
                        break;
                    case ind_state_l:
                        veh.IsRightIndicatorLightOn = false;
                        veh.IsLeftIndicatorLightOn = true;
                        break;
                    case ind_state_r:
                        veh.IsRightIndicatorLightOn = true;
                        veh.IsLeftIndicatorLightOn = false;
                        break;
                    case ind_state_h:
                        veh.IsRightIndicatorLightOn = true;
                        veh.IsLeftIndicatorLightOn = true;
                        break;
                }
                state_indic[veh] = newstate;
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    //Сирена
    void TogMuteDfltSrnForVeh(Vehicle veh, bool toggle)
    {
        if (veh.Exists() || veh.IsAlive) veh.IsSirenSilent = toggle;
    }

    void SetLxSirenStateForVeh(Vehicle veh, int newstate)
    {
        try
        {
            if (veh.Exists() || veh.IsAlive)
            {
                if (newstate != state_lxsiren[veh])
                {
                    if (snd_lxsiren.ContainsKey(veh))
                    {
                        nowAudio.StopSound(snd_lxsiren[veh]);
                        nowAudio.ReleaseSound(snd_lxsiren[veh]);
                        snd_lxsiren.Remove(veh);
                    }
                    if (newstate == 1)
                    {
                        if (useFiretruckSiren(veh)) TogMuteDfltSrnForVeh(veh, false);
                        else
                        {
                            snd_lxsiren[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_SIREN_1");
                            TogMuteDfltSrnForVeh(veh, true);
                        }
                    }
                    else if (newstate == 2)
                    {
                        snd_lxsiren[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_SIREN_2");
                        TogMuteDfltSrnForVeh(veh, true);
                    }
                    else if (newstate == 3)
                    {
                        if (useFiretruckSiren(veh)) snd_lxsiren[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_AMBULANCE_WARNING");
                        else snd_lxsiren[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_POLICE_WARNING");
                        TogMuteDfltSrnForVeh(veh, true);
                    }
                    else TogMuteDfltSrnForVeh(veh, true);
                    state_lxsiren[veh] = newstate;
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }

    }

    void TogPowercallStateForVeh(Vehicle veh, bool toggle)
    {
        try
        {
            if (veh.Exists() || veh.IsAlive)
            {
                if (toggle)
                {
                    if (snd_pwrcall.ContainsKey(veh))
                    {
                        if (usePowercallAuxSrn(veh)) snd_pwrcall[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_AMBULANCE_WARNING");
                        else snd_pwrcall[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_SIREN_1");
                    }
                }
                else
                {
                    if (snd_pwrcall.ContainsKey(veh))
                    {
                        nowAudio.StopSound(snd_pwrcall[veh]);
                        nowAudio.ReleaseSound(snd_pwrcall[veh]);
                        snd_pwrcall.Remove(veh);
                    }
                }
                state_pwrcall[veh] = toggle;
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    void SetAirManuStateForVeh(Vehicle veh, int newstate)
    {
        try
        {
            if (veh.Exists() || veh.IsAlive)
            {
                if (newstate != state_airmanu[veh])
                {
                    if (snd_airmanu.ContainsKey(veh))
                    {
                        nowAudio.StopSound(snd_airmanu[veh]);
                        nowAudio.ReleaseSound(snd_airmanu[veh]);
                        snd_airmanu.Remove(veh);
                    }
                    if (newstate == 1)
                    {
                        if (useFiretruckSiren(veh)) snd_airmanu[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_FIRETRUCK_WARNING");
                        else snd_airmanu[veh] = nowAudio.PlaySoundFromEntity(veh, "SIRENS_AIRHORN");
                    }
                    else if (newstate == 2) snd_airmanu[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_SIREN_1");
                    else if (newstate == 3) snd_airmanu[veh] = nowAudio.PlaySoundFromEntity(veh, "VEHICLES_HORNS_SIREN_2");
                    state_airmanu[veh] = newstate;
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    void TogIndicState(Player sender, string triggerName, dynamic data)
    {
        if (triggerName != "TogIndicState") return;
        try
        {
            var ped = sender.Character;
            if (ped.Exists() && ped.IsAlive)
            {
                if (sender != Game.Player)
                {
                    if (ped.IsInVehicle())
                    {
                        TogIndicStateForVeh(ped.CurrentVehicle, (int)data);
                    }
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    void TogDfltSrnMuted(Player sender, string triggerName, dynamic data)
    {
        if (triggerName != "TogDfltSrnMuted") return;
        try
        {
            var ped = sender.Character;
            if (ped.Exists() && ped.IsAlive)
            {
                if (sender != Game.Player)
                {
                    if (ped.IsInVehicle())
                    {
                        TogMuteDfltSrnForVeh(ped.CurrentVehicle, (bool)data);
                    }
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    void SetLxSirenState(Player sender, string triggerName, dynamic data)
    {
        if (triggerName != "SetLxSirenState") return;
        try
        {
            var ped = sender.Character;
            if (ped.Exists() && ped.IsAlive)
            {
                if (sender != Game.Player)
                {
                    if (ped.IsInVehicle())
                    {
                        SetLxSirenStateForVeh(ped.CurrentVehicle, (int)data);
                    }
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    void TogPwrcallState(Player sender, string triggerName, dynamic data)
    {
        if (triggerName != "TogPwrcallState") return;
        try
        {
            var ped = sender.Character;
            if (ped.Exists() && ped.IsAlive)
            {
                if (sender != Game.Player)
                {
                    if (ped.IsInVehicle())
                    {
                        TogPowercallStateForVeh(ped.CurrentVehicle, (bool)data);
                    }
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    private void SetAirManuState(Player sender, string triggerName, dynamic data)
    {
        if (triggerName != "SetAirManuState") return;
        try
        {
            var ped = sender.Character;
            if (ped.Exists() && ped.IsAlive)
            {
                if (sender != Game.Player)
                {
                    if (ped.IsInVehicle())
                    {
                        SetAirManuStateForVeh(ped.CurrentVehicle, (int)data);
                    }
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
    }

    public Init()
    {
        Network.onNetworkData += TogIndicState;
        Network.onNetworkData += TogDfltSrnMuted;
        Network.onNetworkData += SetLxSirenState;
        Network.onNetworkData += TogPwrcallState;
        Network.onNetworkData += SetAirManuState;
        Tick += Update;
    }

    Vehicle lastVehicle;
    VehicleClass vehClass;
    public Task Update()
    {
        try
        {
            CleanupSounds();
            //----- IS IN VEHICLE -----
            var ped = Game.PlayerPed;
            if (ped.IsInVehicle())
            {
                //----- IS DRIVER -----
                var veh = ped.CurrentVehicle;
                if (lastVehicle != veh)
                {
                    lastVehicle = veh;
                    vehClass = (VehicleClass)Function.Call<int>(Hash.GET_VEHICLE_CLASS, veh.Handle);
                }

                if (veh.Driver == ped)
                {
                    Game.DisableControlThisFrame(0, Control.VehiclePrevRadioTrack); //INPUT_VEH_PREV_RADIO_TRACK 84
                    Game.DisableControlThisFrame(0, Control.VehicleNextRadioTrack); //INPUT_VEH_NEXT_RADIO_TRACK 83

                    if (!state_indic.ContainsKey(veh)) state_indic[veh] = 0;
                    if (!state_lxsiren.ContainsKey(veh)) state_lxsiren[veh] = 0;
                    if (!state_pwrcall.ContainsKey(veh)) state_pwrcall[veh] = false;
                    if (!state_airmanu.ContainsKey(veh)) state_airmanu[veh] = 0;

                    if (!snd_lxsiren.ContainsKey(veh)) snd_lxsiren[veh] = 0;
                    if (!snd_pwrcall.ContainsKey(veh)) snd_pwrcall[veh] = 0;
                    if (!snd_airmanu.ContainsKey(veh)) snd_airmanu[veh] = 0;

                    if (state_indic[veh] != ind_state_o && state_indic[veh] != ind_state_l && state_indic[veh] != ind_state_r && state_indic[veh] != ind_state_h) state_indic[veh] = ind_state_o;

                    //-- INDIC AUTO CONTROL
                    if (actv_ind_timer)
                    {
                        if (state_indic[veh] == ind_state_l || state_indic[veh] == ind_state_r)
                        {
                            if (veh.Speed < 6) count_ind_timer = 0;
                            else
                            {
                                if (count_ind_timer > delay_ind_timer)
                                {
                                    count_ind_timer = 0;
                                    actv_ind_timer = false;
                                    state_indic[veh] = ind_state_o;
                                    nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                    TogIndicStateForVeh(veh, state_indic[veh]);
                                    count_bcast_timer = delay_bcast_timer;
                                }
                                else count_ind_timer = count_ind_timer + 1;
                            }
                        }
                    }

                    //--- IS EMERG VEHICLE ---
                    if (vehClass == VehicleClass.Emergency)
                    {
                        bool actv_manu = false;
                        bool actv_horn = false;
                        Game.DisableControlThisFrame(0, Control.SelectWeaponUnarmed); //1
                        Game.DisableControlThisFrame(0, Control.SelectWeaponMelee); //2
                        Game.DisableControlThisFrame(0, Control.SelectWeaponShotgun); //3
                        Game.DisableControlThisFrame(0, Control.PhoneUp);
                        Game.DisableControlThisFrame(0, Control.PhoneLeft);
                        Game.DisableControlThisFrame(0, Control.VehicleHorn);

                        if (state_lxsiren[veh] != 1 && state_lxsiren[veh] != 2 && state_lxsiren[veh] != 3) state_lxsiren[veh] = 0;
                        if (state_pwrcall[veh] != true) state_pwrcall[veh] = false;
                        if (state_airmanu[veh] != 1 && state_airmanu[veh] != 2 && state_airmanu[veh] != 3) state_airmanu[veh] = 0;
                        if (useFiretruckSiren(veh) && state_lxsiren[veh] == 1) TogMuteDfltSrnForVeh(veh, dsrn_mute = false);
                        else TogMuteDfltSrnForVeh(veh, dsrn_mute = true);
                        if (!veh.IsSirenActive && state_lxsiren[veh] > 0)
                        {
                            nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                            SetLxSirenStateForVeh(veh, 0);
                            count_bcast_timer = delay_bcast_timer;
                        }
                        if (!veh.IsSirenActive && state_pwrcall[veh] == true)
                        {
                            nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                            TogPowercallStateForVeh(veh, false);
                            count_bcast_timer = delay_bcast_timer;
                        }

                        //-----CONTROLS---- -
                        if (!Game.IsPaused)
                        {
                            //TOG DFLT SRN LIGHTS
                            if (Game.IsDisabledControlJustReleased(0, Control.VehicleHorn))
                            {
                                if (veh.IsSirenActive)
                                {
                                    nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                    veh.IsSirenActive = false;
                                }
                                else
                                {
                                    nowAudio.PlaySoundFrontend("NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                    veh.IsSirenActive = true;
                                    count_bcast_timer = delay_bcast_timer;
                                }
                            }

                            //TOG LX SIREN
                            else if (Game.IsDisabledControlJustReleased(0, Control.SelectWeaponUnarmed))
                            {
                                var cstate = state_lxsiren[veh];
                                if (cstate == 0)
                                {
                                    if (veh.IsSirenActive)
                                    {
                                        nowAudio.PlaySoundFrontend("NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                        SetLxSirenStateForVeh(veh, 1);
                                        count_bcast_timer = delay_bcast_timer;
                                    }
                                }
                                else
                                {
                                    nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                    SetLxSirenStateForVeh(veh, 0);
                                    count_bcast_timer = delay_bcast_timer;
                                }
                            }

                            //POWERCALL
                            else if (Game.IsDisabledControlJustReleased(0, Control.SelectWeaponMelee))
                            {
                                if (state_pwrcall[veh] == true)
                                {
                                    nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                    TogPowercallStateForVeh(veh, false);
                                    count_bcast_timer = delay_bcast_timer;
                                }
                                else
                                {
                                    if (veh.IsSirenActive)
                                    {
                                        nowAudio.PlaySoundFrontend("NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                        TogPowercallStateForVeh(veh, true);
                                        count_bcast_timer = delay_bcast_timer;
                                    }
                                }
                            }

                            //BROWSE LX SRN TONES
                            if (state_lxsiren[veh] > 0)
                            {
                                if (Game.IsDisabledControlJustReleased(0, Control.SelectWeaponShotgun))
                                {
                                    if (veh.IsSirenActive)
                                    {
                                        var cstate = state_lxsiren[veh];
                                        var nstate = 1;
                                        nowAudio.PlaySoundFrontend("NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                        if (cstate == 1) nstate = 2;
                                        else if (cstate == 2) nstate = 3;
                                        else nstate = 1;
                                        SetLxSirenStateForVeh(veh, nstate);
                                        count_bcast_timer = delay_bcast_timer;
                                    }
                                }
                            }

                            //MANU
                            if (state_lxsiren[veh] < 1) actv_manu = Game.IsDisabledControlPressed(0, Control.PhoneUp) ? true : false;
                            else actv_manu = false;

                            //HORN
                            if (Game.IsDisabledControlPressed(0, Control.PhoneLeft)) actv_horn = true;
                            else actv_horn = false;
                        }

                        //----ADJUST HORN / MANU STATE----
                        var hmanu_state_new = 0;
                        if (actv_horn == true && actv_manu == false) hmanu_state_new = 1;
                        else if (actv_horn == false && actv_manu == true) hmanu_state_new = 2;
                        else if (actv_horn == true && actv_manu == true) hmanu_state_new = 3;
                        if (hmanu_state_new == 1)
                        {
                            if (!useFiretruckSiren(veh))
                            {
                                if (state_lxsiren[veh] > 0 && actv_lxsrnmute_temp == false)
                                {
                                    srntone_temp = state_lxsiren[veh];
                                    SetLxSirenStateForVeh(veh, 0);
                                    actv_lxsrnmute_temp = true;
                                }
                            }
                        }
                        else
                        {
                            if (!useFiretruckSiren(veh))
                            {
                                if (actv_lxsrnmute_temp == true)
                                {
                                    SetLxSirenStateForVeh(veh, srntone_temp);
                                    actv_lxsrnmute_temp = false;
                                }
                            }
                        }
                        if (state_airmanu[veh] != hmanu_state_new)
                        {
                            SetAirManuStateForVeh(veh, hmanu_state_new);
                            count_bcast_timer = delay_bcast_timer;
                        }


                    }


                    //--- IS ANY LAND VEHICLE ---
                    if (vehClass != VehicleClass.Boats && vehClass != VehicleClass.Helicopters && vehClass != VehicleClass.Planes && vehClass != VehicleClass.Trains)
                    {
                        //----- CONTROLS -----
                        if (!Game.IsPaused)
                        {
                            //IND L
                            if (Game.IsDisabledControlJustReleased(0, Control.VehiclePrevRadioTrack))//INPUT_VEH_PREV_RADIO_TRACK
                            {
                                var cstate = state_indic[veh];
                                if (cstate == ind_state_l)
                                {
                                    state_indic[veh] = ind_state_o;
                                    actv_ind_timer = false;
                                    nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                }
                                else
                                {
                                    state_indic[veh] = ind_state_l;
                                    actv_ind_timer = true;
                                    nowAudio.PlaySoundFrontend("NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                }
                                TogIndicStateForVeh(veh, state_indic[veh]);
                                count_ind_timer = 0;
                                count_bcast_timer = delay_bcast_timer;
                            }
                            else if (Game.IsDisabledControlJustReleased(0, Control.VehicleNextRadioTrack))
                            {
                                var cstate = state_indic[veh];
                                if (cstate == ind_state_r)
                                {
                                    state_indic[veh] = ind_state_o;
                                    actv_ind_timer = false;
                                    nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                }
                                else
                                {
                                    state_indic[veh] = ind_state_r;
                                    actv_ind_timer = true;
                                    nowAudio.PlaySoundFrontend("NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                }
                                TogIndicStateForVeh(veh, state_indic[veh]);
                                count_ind_timer = 0;
                                count_bcast_timer = delay_bcast_timer;
                            }
                            else if (Game.IsControlJustReleased(0, Control.FrontendCancel))
                            {
                                var cstate = state_indic[veh];
                                if (cstate == ind_state_h)
                                {
                                    state_indic[veh] = ind_state_o;
                                    nowAudio.PlaySoundFrontend("NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                }
                                else
                                {
                                    state_indic[veh] = ind_state_h;
                                    nowAudio.PlaySoundFrontend("NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                                }
                                TogIndicStateForVeh(veh, state_indic[veh]);
                                actv_ind_timer = false;
                                count_ind_timer = 0;
                                count_bcast_timer = delay_bcast_timer;
                            }
                        }

                        //-----AUTO BROADCAST VEH STATES---- -
                        if (count_bcast_timer > delay_bcast_timer)
                        {
                            count_bcast_timer = 0;
                            //--- IS EMERG VEHICLE ---
                            if (Function.Call<int>(Hash.GET_VEHICLE_CLASS, veh.Handle) == (int)VehicleClass.Emergency)
                            {
                                Network.Send("TogDfltSrnMuted", dsrn_mute);
                                Network.Send("SetLxSirenState", state_lxsiren[veh]);
                                Network.Send("TogPwrcallState", state_pwrcall[veh]);
                                Network.Send("SetAirManuState", state_airmanu[veh]);
                            }
                            Network.Send("TogIndicState", state_indic[veh]);
                        }
                        else count_bcast_timer = count_bcast_timer + 1;
                    }
                }
            }
        }
        catch (Exception e) { Debug.WriteLine(e.Message); }
        return Task.FromResult(0);
    }
}