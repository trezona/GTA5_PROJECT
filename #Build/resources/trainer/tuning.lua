local neoncolor, vehiclecol, extracol
local tireproof = false
local turbotoggle = false
local xenontoggle = false
local neonleft = false
local neonright = false
local neonfront = false
local neonrear = false
local rainbowneons = false
local rainbowcar = false
local rainbowtire = false
local plateText = "PLACEHOLDER"
local R = 0
local G = 0
local B = 0


RegisterNUICallback("armor", function(data, cb) --Armor Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 16) - 1)) then
			SetVehicleMod(playerVeh, 16, modIndex)
			drawNotification("~g~Tuned Vehicle Armor!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("brakes", function(data, cb) --Brakes Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 12) - 1)) then
			SetVehicleMod(playerVeh, 12, modIndex)
			drawNotification("~g~Tuned Vehicle Brakes!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("bumperF", function(data, cb) --Front Bumper Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 1) - 1)) then
			SetVehicleMod(playerVeh, 1, modIndex)
			drawNotification("~g~Tuned Vehicle Front Bumper!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("bumperR", function(data, cb) --Rear Bumper Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 2) - 1)) then
			SetVehicleMod(playerVeh, 2, modIndex)
			drawNotification("~g~Tuned Vehicle Rear Bumper!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("color", function(data, cb) --Color Menu

	if data.action == "rainbow" then --Rainbow Car
		rainbowcar = data.newstate
		if (rainbowcar == true) then
			drawNotification("~g~Rainbow Car Enabled!")
		else
			drawNotification("~r~Rainbow Car Disabled!")
		end
	end

	cb("ok")
end)

RegisterNUICallback("engine", function(data, cb) --Engine Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 11) - 1)) then
			SetVehicleMod(playerVeh, 11, modIndex)
			drawNotification("~g~Tuned Vehicle Engine!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("exhaust", function(data, cb) --Exhaust Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 4) - 1)) then
			SetVehicleMod(playerVeh, 4, modIndex)
			drawNotification("~g~Tuned Vehicle Exhaust!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("fender", function(data, cb) --Fender Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 8) - 1)) then
			SetVehicleMod(playerVeh, 8, modIndex)
			drawNotification("~g~Tuned Vehicle Fender!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("fenderR", function(data, cb) --Fender (Right) Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 9) - 1)) then
			SetVehicleMod(playerVeh, 9, modIndex)
			drawNotification("~g~Tuned Vehicle Right Fender!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("frame", function(data, cb) --Frame Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 5) - 1)) then
			SetVehicleMod(playerVeh, 5, modIndex)
			drawNotification("~g~Tuned Vehicle Frame!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("grille", function(data, cb) --Grille Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 6) - 1)) then
			SetVehicleMod(playerVeh, 6, modIndex)
			drawNotification("~g~Tuned Vehicle Grille!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("hood", function(data, cb) --Hood Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 7) - 1)) then
			SetVehicleMod(playerVeh, 7, modIndex)
			drawNotification("~g~Tuned Vehicle Hood!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("horn", function(data, cb) --Horn Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local hornIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (hornIndex <= (GetNumVehicleMods(playerVeh, 14) - 0)) then
			SetVehicleMod(playerVeh, 14, hornIndex)
			drawNotification("~g~Tuned Vehicle Horn!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("light", function(data, cb) --Light Menu

	if data.action == "xenon" then --Xenon ON/ OFF Switch
		xenontoggle = data.newstate
		if (xenontoggle == true) then
			drawNotification("~g~Xenon Lights Enabled!")
		else
			drawNotification("~r~Xenon Lights Disabled!")
		end
	end
	
end)

RegisterNUICallback("neons", function(data, cb) --Vehicle Neons
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local action = data.action
	local newstate = data.newstate

	if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
		if action == "left" then
			neonleft = newstate
			if (neonleft == true) then
				drawNotification("~g~Left Neon Enabled!")
			else
				drawNotification("~r~Left Neon Disabled!")
			end
		elseif action == "right" then
			neonright = newstate
			if (neonright == true) then
				drawNotification("~g~Right Neon Enabled!")
			else
				drawNotification("~r~Right Neon Disabled!")
			end
		elseif action == "front" then
			neonfront = newstate
			if (neonfront == true) then
				drawNotification("~g~Front Neon Enabled!")
			else
				drawNotification("~r~Front Neon Disabled!")
			end
		elseif action == "rear" then
			neonrear = newstate
			if (neonrear == true) then
				drawNotification("~g~Back Neon Enabled!")
			else
				drawNotification("~r~Back Neon Disabled!")
			end
		elseif action == "rainbow" then --Rainbow Neons
			rainbowneons = newstate
			if (rainbowneons == true) then
				drawNotification("~g~Rainbow Neons Enabled!")
			else
				drawNotification("~r~Rainbow Neons Disabled!")
			end
		elseif action == "custom" then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end
			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				local color = stringsplit(result, ",")
				R = tonumber(color[1])
				G = tonumber(color[2])
				B = tonumber(color[3])
				Citizen.Wait(500)
				blockinput = false
				SetVehicleNeonLightsColour(playerVeh, R, G, B)
				drawNotification("~g~Changed Neon Color")
			else
				Citizen.Wait(500)
				blockinput = false
			end
		elseif action == "white" then
			SetVehicleNeonLightsColour(playerVeh, 255, 255, 255)
		elseif action == "blue" then
			SetVehicleNeonLightsColour(playerVeh, 0, 0, 255)
		elseif action == "electricblue" then
			SetVehicleNeonLightsColour(playerVeh, 0, 150, 255)
		elseif action == "mintgreen" then
			SetVehicleNeonLightsColour(playerVeh, 50, 255, 155)
		elseif action == "limegreen" then
			SetVehicleNeonLightsColour(playerVeh, 0, 255, 0)
		elseif action == "yellow" then
			SetVehicleNeonLightsColour(playerVeh, 255, 255, 0)
		elseif action == "orange" then
			SetVehicleNeonLightsColour(playerVeh, 208, 104, 0)
		elseif action == "red" then
			SetVehicleNeonLightsColour(playerVeh, 255, 0, 0)
		elseif action == "ponypink" then
			SetVehicleNeonLightsColour(playerVeh, 255, 102, 255)
		elseif action == "hotpink" then
			SetVehicleNeonLightsColour(playerVeh, 255, 0, 255)
		elseif action == "purple" then
			SetVehicleNeonLightsColour(playerVeh, 71, 0, 71)
		end
	else
		drawNotification("~r~Not in a vehicle!")
	end
	
	cb("ok")
end)

RegisterNUICallback("plate", function(data, cb) --Plate Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local plateIndex = tonumber(data.action)
	
	if data.action == "text" then
		plateText = GetVehicleNumberPlateText(playerVeh)
		DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. plateText .. "", "", "", "", 25)
		blockinput = true

		while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
			Citizen.Wait(0)
		end
		if UpdateOnscreenKeyboard() ~= 2 then
			plateText = GetOnscreenKeyboardResult()
			Citizen.Wait(500)
			blockinput = false
		else
			Citizen.Wait(500)
			blockinput = false
		end
		SetVehicleNumberPlateText(playerVeh, plateText)
	else
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
			SetVehicleModKit(playerVeh, 0)
			SetVehicleNumberPlateTextIndex(playerVeh, plateIndex)
			drawNotification("~g~Tuned Vehicle Plate!")
		else
			drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
		end
	end
end)

RegisterNUICallback("roof", function(data, cb) --Roof Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 10) - 1)) then
			SetVehicleMod(playerVeh, 10, modIndex)
			drawNotification("~g~Tuned Vehicle Roof!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("sideskirt", function(data, cb) --Side Skirt Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 3) - 1)) then
			SetVehicleMod(playerVeh, 3, modIndex)
			drawNotification("~g~Tuned Vehicle Side Skirt!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("spoiler", function(data, cb) --Spoiler Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 0) - 1)) then
			SetVehicleMod(playerVeh, 0, modIndex)
			drawNotification("~g~Tuned Vehicle Spoiler!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("suspension", function(data, cb) --Suspension Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 15) - 1)) then
			SetVehicleMod(playerVeh, 15, modIndex)
			drawNotification("~g~Tuned Vehicle Suspension!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("tire", function(data, cb) --Tire Menu

	if data.action == "proof" then --Bulletproof
		tireproof = data.newstate
		if (tireproof == true) then
			drawNotification("~g~Bullet Proof Tires Enabled!")
		else
			drawNotification("~r~Bullet Proof Tires Disabled!")
		end
	elseif data.action == "rainbow" then --Rainbow Tiresmoke
		rainbowtire = data.newstate
		if (rainbowtire == true) then
			drawNotification("~g~Rainbow Tire Smoke Enabled!")
		else
			drawNotification("~r~Rainbow Tire Smoke Disabled!")
		end
	elseif data.action == "no" then --No Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 0, 0, 0)
	elseif data.action == "custom" then --Custom Tiresmoke
		DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
		blockinput = true

		while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
			Citizen.Wait(0)
		end
		if UpdateOnscreenKeyboard() ~= 2 then
			local result = GetOnscreenKeyboardResult()
			local color = stringsplit(result, ",")
			R = tonumber(color[1])
			G = tonumber(color[2])
			B = tonumber(color[3])
			Citizen.Wait(500)
			blockinput = false
			ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
			SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), R, G, B)
		else
			Citizen.Wait(500)
			blockinput = false
		end
	elseif data.action == "white" then --White Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 254, 254, 254)
	elseif data.action == "black" then --Black Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 1, 1, 1)
	elseif data.action == "blue" then --Blue Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 0, 0, 255)
	elseif data.action == "yellow" then --Yellow Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 200, 200, 0)
	elseif data.action == "purple" then --Purple Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 153, 0, 153)
	elseif data.action == "orange" then --Orange Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 255, 153, 0)
	elseif data.action == "green" then --Green Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 0, 255, 0)
	elseif data.action == "red" then --Red Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 255, 0, 0)
	elseif data.action == "pink" then --Pink Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 255, 102, 153)
	elseif data.action == "brown" then --Brown Tiresmoke
		ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
		SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), 128, 43, 0)
	end
	
end)

RegisterNUICallback("transmission", function(data, cb) --Transmission Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (modIndex <= (GetNumVehicleMods(playerVeh, 13) - 1)) then
			SetVehicleMod(playerVeh, 13, modIndex)
			drawNotification("~g~Tuned Vehicle Transmission!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("turbo", function(data, cb) --Turbo Menu

	if data.action == "onoff" then --Turbo
		turbotoggle = data.newstate
		if (turbotoggle == true) then
			drawNotification("~g~Turbo Enabled!")
		else
			drawNotification("~r~Turbo Disabled!")
		end
	end
	
end)

RegisterNUICallback("wheels", function(data, cb) --Wheels Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local modIndex = stringsplit(data.action, ",")
	local wheelType = tonumber(modIndex[1])
	local wheel = tonumber(modIndex[2])
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (wheel <= (GetNumVehicleMods(playerVeh, 23) - 1)) then
			SetVehicleWheelType(playerVeh, wheelType)
			SetVehicleMod(playerVeh, 23, wheel, true)
			drawNotification("~g~Tuned Vehicle Wheels!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("window", function(data, cb) --Window Tint Menu
	local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
	local windowTint = tonumber(data.action)
	
	if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(playerVeh, -1) == GetPlayerPed(-1)) then
		SetVehicleModKit(playerVeh, 0)
		if (windowTint <= GetNumVehicleWindowTints()) then
			SetVehicleWindowTint(playerVeh, windowTint)
			drawNotification("~g~Tuned Vehicle Window Tint!")
		else
			drawNotification("~r~Not Available For This Vehicle!")
		end
	else
		drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
	end
end)

RegisterNUICallback("primatte", function(data, cb) --Vehicle Color (Primary Matte)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)

	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleModColor_1(playerVeh, 3)
		ClearVehicleCustomPrimaryColour(playerVeh)
		if data.action == "custom" then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end
			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				local color = stringsplit(result, ",")
				R = tonumber(color[1])
				G = tonumber(color[2])
				B = tonumber(color[3])
				Citizen.Wait(500)
				blockinput = false
				SetVehicleCustomPrimaryColour(playerVeh, R, G, B)
				SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
			else
				Citizen.Wait(500)
				blockinput = false
			end
		else
			SetVehicleColours(playerVeh, colorindex, vehiclecol[2])
			SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
		end
		drawNotification("~g~Repainted Primary Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

RegisterNUICallback("primetalchrome", function(data, cb) --Vehicle Color (Primary Metals & Chrome)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)
	local modColor

	if colorindex == 120 or data.action == "custom" then
		modColor = 5
	else
		modColor = 4
	end
	
	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleModColor_1(playerVeh, modColor)
		if data.action == "custom" then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end
			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				local color = stringsplit(result, ",")
				R = tonumber(color[1])
				G = tonumber(color[2])
				B = tonumber(color[3])
				Citizen.Wait(500)
				blockinput = false
				SetVehicleCustomPrimaryColour(playerVeh, R, G, B)
				SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
			else
				Citizen.Wait(500)
				blockinput = false
			end
		else
			ClearVehicleCustomPrimaryColour(playerVeh)
			SetVehicleColours(playerVeh, colorindex, vehiclecol[2])
			SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
		end
		drawNotification("~g~Repainted Primary Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

RegisterNUICallback("primetallic", function(data, cb) --Vehicle Color (Primary Metallic)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)

	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleModColor_1(playerVeh, 1)
		ClearVehicleCustomPrimaryColour(playerVeh)
		if data.action == "custom" then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end
			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				local color = stringsplit(result, ",")
				R = tonumber(color[1])
				G = tonumber(color[2])
				B = tonumber(color[3])
				Citizen.Wait(500)
				blockinput = false
				SetVehicleCustomPrimaryColour(playerVeh, R, G, B)
				SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
			else
				Citizen.Wait(500)
				blockinput = false
			end
		else
			SetVehicleColours(playerVeh, colorindex, vehiclecol[2])
			SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
		end
		drawNotification("~g~Repainted Primary Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

RegisterNUICallback("secmatte", function(data, cb) --Vehicle Color (Secondary Matte)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)

	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleModColor_2(playerVeh, 3)
		ClearVehicleCustomSecondaryColour(playerVeh)
		if data.action == "custom" then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end
			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				local color = stringsplit(result, ",")
				R = tonumber(color[1])
				G = tonumber(color[2])
				B = tonumber(color[3])
				Citizen.Wait(500)
				blockinput = false
				SetVehicleCustomSecondaryColour(playerVeh, R, G, B)
				SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
			else
				Citizen.Wait(500)
				blockinput = false
			end
		else
			SetVehicleColours(playerVeh, vehiclecol[1], colorindex)
			SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
		end
		drawNotification("~g~Repainted Secondary Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

RegisterNUICallback("secmetalchrome", function(data, cb) --Vehicle Color (Secondary Metals & Chrome)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)
	local modColor

	if colorindex == 120 or data.action == "custom" then
		modColor = 5
	else
		modColor = 4
	end
	
	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleModColor_2(playerVeh, modColor)
		if data.action == "custom" then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end
			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				local color = stringsplit(result, ",")
				R = tonumber(color[1])
				G = tonumber(color[2])
				B = tonumber(color[3])
				Citizen.Wait(500)
				blockinput = false
				SetVehicleCustomSecondaryColour(playerVeh, R, G, B)
				SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
			else
				Citizen.Wait(500)
				blockinput = false
			end
		else
			ClearVehicleCustomSecondaryColour(playerVeh)
			SetVehicleColours(playerVeh, vehiclecol[1], colorindex)
			SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
		end
		drawNotification("~g~Repainted Secondary Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

RegisterNUICallback("secmetallic", function(data, cb) --Vehicle Color (Secondary Metallic)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)

	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleModColor_2(playerVeh, 1)
		ClearVehicleCustomSecondaryColour(playerVeh)
		if data.action == "custom" then
			DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "" .. R .. "," .. G .. "," .. B .. "", "", "", "", 25)
			blockinput = true

			while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
				Citizen.Wait(0)
			end
			if UpdateOnscreenKeyboard() ~= 2 then
				local result = GetOnscreenKeyboardResult()
				local color = stringsplit(result, ",")
				R = tonumber(color[1])
				G = tonumber(color[2])
				B = tonumber(color[3])
				Citizen.Wait(500)
				blockinput = false
				SetVehicleCustomSecondaryColour(playerVeh, R, G, B)
				SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
			else
				Citizen.Wait(500)
				blockinput = false
			end
		else
			SetVehicleColours(playerVeh, vehiclecol[1], colorindex)
			SetVehicleExtraColours(playerVeh, extracol[1], extracol[2])
		end
		drawNotification("~g~Repainted Secondary Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

RegisterNUICallback("pearl", function(data, cb) --Vehicle Color (Pearlescent)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)

	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleExtraColours(playerVeh, colorindex, extracol[2])
		drawNotification("~g~Repainted Pearlescent Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

RegisterNUICallback("rims", function(data, cb) --Vehicle Color (Rims)
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local colorindex = tonumber(data.action)

	if IsPedInAnyVehicle(playerPed, true) then
		SetVehicleExtraColours(playerVeh, extracol[1], colorindex)
		drawNotification("~g~Repainted Rims Color")
	else
		drawNotification("~r~Not in a vehicle!")
	end
	cb("ok")
end)

Citizen.CreateThread(function() --Bulletproof Tires
	while true do
		Citizen.Wait(0)
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			if (tireproof == true) then
				SetVehicleTyresCanBurst(GetVehiclePedIsIn(GetPlayerPed(-1), false), false)
			elseif (tireproof == false) then
				SetVehicleTyresCanBurst(GetVehiclePedIsIn(GetPlayerPed(-1), false), true)
			end
		end
	end
end)

Citizen.CreateThread(function() --Xenon
	while true do
		Citizen.Wait(0)
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			if (xenontoggle == true) then
				ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 22, true)
			elseif (xenontoggle == false) then
				ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 22, false)
			end
		end
	end
end)

Citizen.CreateThread(function() --Turbo
	while true do
		Citizen.Wait(0)
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			if (turbotoggle == true) then
				ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 18, true)
			elseif (turbotoggle == false) then
				ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 18, false)
			end
		end
	end
end)

Citizen.CreateThread(function() --Neons
	while true do
		Citizen.Wait(0)
		if (neonleft == true) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 0, true)
		elseif (neonleft == false) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 0, false)
		end
		if (neonright == true) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 1, true)
		elseif (neonright == false) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 1, false)
		end
		if (neonfront == true) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 2, true)
		elseif (neonfront == false) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 2, false)
		end
		if (neonrear == true) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 3, true)
		elseif (neonrear == false) then
			SetVehicleNeonLightEnabled(GetVehiclePedIsIn(GetPlayerPed(-1)), 3, false)
		end
	end
end)

Citizen.CreateThread(function() --Rainbow Neons
	while true do
		Citizen.Wait(0)
		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		local rgb = RGBRainbow(0.825)
		if (rainbowneons == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				SetVehicleNeonLightsColour(playerVeh, rgb.r, rgb.g, rgb.b)
				Citizen.Wait(350)
			end
		end
	end
end)

Citizen.CreateThread(function() --Rainbow Car
	while true do
		Citizen.Wait(0)
		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		local rgb = RGBRainbow(0.825)
		if (rainbowcar == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				SetVehicleCustomPrimaryColour(playerVeh, rgb.r, rgb.g, rgb.b)
				SetVehicleCustomSecondaryColour(playerVeh, rgb.r, rgb.g, rgb.b)
				Citizen.Wait(350)
			end
		end
	end
end)

Citizen.CreateThread(function() --Rainbow Tire Smoke
	while true do
		Citizen.Wait(0)
		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		local rgb = RGBRainbow(0.825)
		if (rainbowtire == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				ToggleVehicleMod(GetVehiclePedIsIn(GetPlayerPed(-1), false), 20, true)
				SetVehicleTyreSmokeColor(GetVehiclePedIsIn(GetPlayerPed(-1), false), rgb.r, rgb.g, rgb.b)
				Citizen.Wait(350)
			end
		end
	end
end)

Citizen.CreateThread(function() --Get Current Vehicle Neon Color (Vehicle Color Menu)
	while true do
		Citizen.Wait(0)
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			vehiclecol = table.pack(GetVehicleColours(GetVehiclePedIsIn(GetPlayerPed(-1), false)))
			extracol = table.pack(GetVehicleExtraColours(GetVehiclePedIsIn(GetPlayerPed(-1), false)))
			neoncolor = table.pack(GetVehicleNeonLightsColour(GetVehiclePedIsIn(GetPlayerPed(-1), false)))
		end
	end
end)

function RGBRainbow(frequency) --Rainbow Function (Credits to Ash)
    local result = {}

    result.r = math.floor(math.sin((GetGameTimer() / 5000) * frequency + 0) * 127 + 128)
    result.g = math.floor(math.sin((GetGameTimer() / 5000) * frequency + 2) * 127 + 128)
    result.b = math.floor(math.sin((GetGameTimer() / 5000) * frequency + 4) * 127 + 128)

    return result
end